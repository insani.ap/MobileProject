package com.project.nex.model.localdb.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.project.nex.model.localdb.dao.CustomerDAO;
import com.project.nex.model.localdb.dao.RoutePlanCustomerDAO;
import com.project.nex.model.localdb.dao.RoutePlanDAO;
import com.project.nex.model.localdb.dao.SurveyAnswerDAO;
import com.project.nex.model.localdb.dao.SurveyDAO;
import com.project.nex.model.localdb.dao.SurveyReportDAO;
import com.project.nex.model.localdb.dao.TaggingDAO;
import com.project.nex.model.localdb.dao.TodayDateTimeDAO;
import com.project.nex.model.localdb.dao.UserDAO;
import com.project.nex.model.localdb.entity.Customer;
import com.project.nex.model.localdb.entity.RoutePlan;
import com.project.nex.model.localdb.entity.Survey;
import com.project.nex.model.localdb.entity.SurveyAnswer;
import com.project.nex.model.localdb.entity.Tagging;
import com.project.nex.model.localdb.entity.TodayDateTime;
import com.project.nex.model.localdb.entity.User;

@Database(entities = {User.class, Customer.class, RoutePlan.class, Tagging.class, Survey.class, SurveyAnswer.class, TodayDateTime.class}, version = 1)
public abstract class NexDatabase extends RoomDatabase {
    public abstract UserDAO userDAO();
    public abstract CustomerDAO customerDAO();
    public abstract RoutePlanDAO routePlanDAO();
    public abstract RoutePlanCustomerDAO routePlanCustomerDAO();
    public abstract TaggingDAO taggingDAO();
    public abstract SurveyDAO surveyDAO();
    public abstract SurveyAnswerDAO surveyAnswerDAO();
    public abstract SurveyReportDAO surveyReportDAO();
    public abstract TodayDateTimeDAO todayDateTimeDAO();
}