package com.project.nex.model.repository.Tagging;

import android.content.Context;
import android.graphics.Bitmap;

import androidx.room.Room;

import com.google.gson.Gson;
import com.project.nex.model.localdb.database.NexDatabase;
import com.project.nex.model.localdb.entity.Tagging;
import com.project.nex.model.repository.Retrofit.Config.MasterApi;
import com.project.nex.model.repository.Retrofit.Config.RetrofitConf;
import com.project.nex.model.repository.Retrofit.pojo.TaggingRetrofit;
import com.project.nex.model.repository.RoutePlan.RoutePlanRepository;
import com.project.nex.util.ImageHelper;
import com.project.nex.util.ProgressBarHelper;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TaggingRepository {
    private final NexDatabase nexDatabase;
    private final MasterApi masterApi;
    private final Context context;
    private String date;

    public TaggingRepository(Context context) {
        nexDatabase = Room.databaseBuilder(context, NexDatabase.class, "dbNex").allowMainThreadQueries().build();
        masterApi = RetrofitConf.init().create(MasterApi.class);
        this.context = context;
        date = nexDatabase.todayDateTimeDAO().getDate();
    }

    public String saveTagging(Bitmap photo, String photoName, String serialNumber, int contractNumber, String date, String idRoutePlan) {
        photoName = photoName+"_"+nexDatabase.customerDAO().getDetailsCustomer(nexDatabase.routePlanDAO().getOneRoutePlan(idRoutePlan).getCustomerId()).getEmail()+"_"+nexDatabase.userDAO().getUser().getUsername()+"_"+idRoutePlan+".png";
        if (ImageHelper.saveImage(photo, photoName).equalsIgnoreCase("OK")) {
            Tagging tagging = new Tagging(0, serialNumber, photoName, contractNumber, date, nexDatabase.todayDateTimeDAO().getDate(), false, nexDatabase.routePlanDAO().getOneRoutePlan(idRoutePlan).getCustomerId(), Integer.parseInt(idRoutePlan));
            try {
                nexDatabase.taggingDAO().insertTagging(tagging);
                nexDatabase.routePlanDAO().updateStatusRoutePlan("Finished", Integer.parseInt(idRoutePlan));
                return "OK";
            } catch (Exception e) {
                return "Error";
            }
        }
        else {
            return "Error";
        }
    }

    public Tagging getTagging(int id) {
        return nexDatabase.taggingDAO().getTaggingDetails(id);
    }

    public void syncToApi(TaggingRepositoryCallback taggingRepositoryCallback) {
        try {
            ProgressBarHelper progressBarHelper = new ProgressBarHelper(context);
            progressBarHelper.start();
            List<MultipartBody.Part> images = new ArrayList<>();
            List<TaggingRetrofit> taggingRetrofitList = new ArrayList<>();
            List<Tagging> taggingList = new ArrayList<>();
            List<Integer> idRoutePlanFinished = nexDatabase.routePlanDAO().getAllRoutePlanIdFinished(date);
            if (idRoutePlanFinished.size() != 0) {
                for (int i = 0; i < idRoutePlanFinished.size(); i++) {
                    taggingList.add(nexDatabase.taggingDAO().getTaggingDetails(idRoutePlanFinished.get(i)));
                }
                for (Tagging tagging : taggingList) {
                    taggingRetrofitList.add(new TaggingRetrofit(
                            0,
                            tagging.getSerialNumber(),
                            tagging.getUrlPhoto(),
                            tagging.getNomerKontrak(),
                            tagging.getTglNonaktifAsset(),
                            date,
                            false,
                            nexDatabase.customerDAO().getDetailsCustomer(tagging.getCustomerId()),
                            nexDatabase.userDAO().getUser()
                    ));
                    images.add(taggingFile(tagging.getUrlPhoto()));
                }

                Gson gson = new Gson();
                RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), gson.toJson(taggingRetrofitList));

                Call<String> call = masterApi.saveRoutePlanTagging(requestBody, images);
                call.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        if (response.body().equalsIgnoreCase("true")) {
                            RoutePlanRepository routePlanRepository = new RoutePlanRepository(context);
                            for (int i = 0; i < idRoutePlanFinished.size(); i++) {
                                routePlanRepository.updateStatusSynced(idRoutePlanFinished.get(i));
                                routePlanRepository.updateRoutePlanStatus(idRoutePlanFinished.get(i));
                            }
                            progressBarHelper.end();
                            taggingRepositoryCallback.onSuccess();
                        }
                        else {
                            progressBarHelper.end();
                            taggingRepositoryCallback.onError("Error");
                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        progressBarHelper.end();
                        taggingRepositoryCallback.onError("Error");
                    }
                });
            }
            else {
                progressBarHelper.end();
                taggingRepositoryCallback.onError("No Need For Sync Data");
            }
        } catch (Exception e) {

        }
    }

    public MultipartBody.Part taggingFile(String path) {
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), ImageHelper.getImageFix(path));
        return MultipartBody.Part.createFormData("image", ImageHelper.getImageFix(path).getName(), requestFile);
    }
}
