package com.project.nex.view.Home.Dashboard.ProfilePicture;

import android.os.Bundle;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.project.nex.R;
import com.project.nex.util.ConnectionHelper;
import com.squareup.picasso.Picasso;

public class ImagePreviewUser extends AppCompatActivity {
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview_image);
        imageView = findViewById(R.id.image_large);
        if (null == getIntent().getStringExtra("image")) {
            imageView.setImageDrawable(getResources().getDrawable(R.mipmap.ic_launcher_round));
        }
        else {
            loadImage(getIntent().getStringExtra("image"));
        }
    }

    public void loadImage(String urlPhoto) {
        Picasso.get().load(urlPhoto.replace("http://localhost:8080/",ConnectionHelper.URL)).into(imageView);
    }
}
