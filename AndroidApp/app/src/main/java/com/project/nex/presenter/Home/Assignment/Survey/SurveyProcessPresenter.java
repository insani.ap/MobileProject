package com.project.nex.presenter.Home.Assignment.Survey;

import android.content.Context;
import android.text.TextUtils;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import androidx.room.Room;

import com.project.nex.model.localdb.database.NexDatabase;
import com.project.nex.model.localdb.entity.Survey;
import com.project.nex.model.localdb.entity.SurveyAnswer;
import com.project.nex.model.repository.RoutePlan.RoutePlanRepository;
import com.project.nex.model.repository.Survey.SurveyAnswerRepository;
import com.project.nex.model.repository.Survey.SurveyRepository;
import com.project.nex.view.Home.Assignment.Survey.SurveyProcessView;

import java.util.ArrayList;
import java.util.List;

public class SurveyProcessPresenter {
    private SurveyProcessView surveyProcessView;
    private Context context;
    private String statusData = "OK";

    public SurveyProcessPresenter(SurveyProcessView surveyProcessView, Context context) {
        this.surveyProcessView = surveyProcessView;
        this.context = context;
    }

    public void processSurvey(LinearLayout linearLayout, int idRoutePlan) {
        SurveyRepository surveyRepository = new SurveyRepository(context);
        List<Survey> surveyList = surveyRepository.getAllLocalSurvey();
        String[] status = new String[surveyList.size()];
        int i = 0;
        int markRadB = 0;
        int markCheckB = 0;

        for (Survey survey : surveyList) {
            if (survey.getType().equalsIgnoreCase("FreeText")) {
                if (freeTextAnswer(survey, linearLayout) == null) {
                    status[i] = "Failed";
                }
                else {
                    status[i] = "OK";
                }
            }
            else if (survey.getType().equalsIgnoreCase("RadioButton")) {
                if (radioButtonAnswer(survey, linearLayout, markRadB) == null) {
                    status[i] = "Failed";
                }
                else {
                    status[i] = "OK";
                }
                markRadB = survey.getSurveyOption().split(";").length;
            }
            else if (survey.getType().equalsIgnoreCase("CheckBox")) {
                if (checkBoxAnswer(survey, linearLayout, markCheckB) == null) {
                    status[i] = "Failed";
                }
                else {
                    status[i] = "OK";
                }
                markCheckB = survey.getSurveyOption().split(";").length;
            }
            i++;
        }

        for (String str: status) {
            if (str.equalsIgnoreCase("Failed")) {
                statusData = "Failed";
                break;
            }
        }

        if (statusData.equalsIgnoreCase("Failed")) {
            surveyProcessView.saveFailed("Error, Data not Complete");
        }
        else {
            int markRad = 0;
            int markCheck = 0;
            List<SurveyAnswer> surveyAnswerList = new ArrayList<>();
            RoutePlanRepository routePlanRepository = new RoutePlanRepository(context);
            NexDatabase nexDatabase = Room.databaseBuilder(context, NexDatabase.class, "dbNex").allowMainThreadQueries().build();
            for (Survey survey : surveyList) {
                if (survey.getType().equalsIgnoreCase("FreeText")) {
                    surveyAnswerList.add(new SurveyAnswer(0, freeTextAnswer(survey, linearLayout), idRoutePlan, survey.getId()));
                }
                else if (survey.getType().equalsIgnoreCase("RadioButton")) {
                    surveyAnswerList.add(new SurveyAnswer(0, radioButtonAnswer(survey, linearLayout, markRad), idRoutePlan, survey.getId()));
                    markRad = survey.getSurveyOption().split(";").length;
                }
                else if (survey.getType().equalsIgnoreCase("CheckBox")) {
                    surveyAnswerList.add(new SurveyAnswer(0, checkBoxAnswer(survey, linearLayout, markCheck), idRoutePlan, survey.getId()));
                    markCheck = survey.getSurveyOption().split(";").length;
                }
            }
            SurveyAnswerRepository surveyAnswerRepository = new SurveyAnswerRepository(context);
            if (surveyAnswerRepository.saveAnswer(surveyAnswerList, idRoutePlan).equalsIgnoreCase("OK")) {
                surveyProcessView.saveOK();
            }
            else {
                surveyProcessView.saveFailed("Something Error");
            }
        }
    }

    public String freeTextAnswer(Survey survey, LinearLayout linearLayout) {
        EditText ans = linearLayout.findViewById(survey.getId()+ SurveyPresenter.ID_FREE_TEXT);
        return TextUtils.isEmpty(ans.getText().toString()) ? null : ans.getText().toString();
    }

    public String radioButtonAnswer(Survey survey, LinearLayout linearLayout, int mark) {
        int length = survey.getSurveyOption().split(";").length;
        String ans = null;
        RadioButton[] radioButtons = new RadioButton[length];
        for (int i = 0; i < length; i++) {
            radioButtons[i] = linearLayout.findViewById(SurveyPresenter.ID_RADIO_BUTTON+mark);
            if (radioButtons[i].isChecked()) {
                ans = radioButtons[i].getText().toString();
            }
            mark++;
        }
        return ans;
    }

    public String checkBoxAnswer(Survey survey, LinearLayout linearLayout, int mark) {
        int length = survey.getSurveyOption().split(";").length;
        StringBuilder ans = new StringBuilder();
        CheckBox[] checkBoxes = new CheckBox[length];
        for (int i = 0; i < length; i++) {
            checkBoxes[i] = linearLayout.findViewById(SurveyPresenter.ID_CHECK_BOX+mark);
            if (checkBoxes[i].isChecked()) {
                ans.append(checkBoxes[i].getText().toString()).append(";");
            }
            mark++;
        }

        return TextUtils.isEmpty(ans) ? null : ans.deleteCharAt(ans.length()-1).toString();
    }
}