package com.project.nex.view.Home.Report;

public interface ReportSyncView {
    void syncSuccess(String message);
    void syncFailed(String message);
}
