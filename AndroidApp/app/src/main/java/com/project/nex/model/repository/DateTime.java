package com.project.nex.model.repository;

import android.content.Context;

import androidx.room.Room;

import com.project.nex.model.localdb.database.NexDatabase;
import com.project.nex.model.localdb.entity.TodayDateTime;
import com.project.nex.util.ConnectionHelper;
import com.project.nex.util.ConnectionHelperCallback;

public class DateTime {
    public static void getTodayDateTime(Context context) {
        ConnectionHelper.getDate(new ConnectionHelperCallback() {
            @Override
            public void onSuccess(String dateNow, Long timeInMilis) {
                ConnectionHelper.getTime(new ConnectionHelperCallback() {
                    @Override
                    public void onSuccess(String date, Long timeInMilis) {
                        NexDatabase nexDatabase = Room.databaseBuilder(context, NexDatabase.class, "dbNex").allowMainThreadQueries().build();
                        nexDatabase.todayDateTimeDAO().insertDateTime(new TodayDateTime(1, dateNow, timeInMilis));
                    }

                    @Override
                    public void onError() {

                    }
                });
            }

            @Override
            public void onError() {

            }
        });
    }
}
