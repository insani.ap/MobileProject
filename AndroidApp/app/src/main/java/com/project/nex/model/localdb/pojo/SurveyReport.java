package com.project.nex.model.localdb.pojo;

public class SurveyReport {
    int survey_id;
    String answer;
    String survey_question;
    String type;
    String survey_option;

    public SurveyReport(int survey_id, String answer, String survey_question, String type, String survey_option) {
        this.survey_id = survey_id;
        this.answer = answer;
        this.survey_question = survey_question;
        this.type = type;
        this.survey_option = survey_option;
    }

    public int getSurvey_id() {
        return survey_id;
    }

    public void setSurvey_id(int survey_id) {
        this.survey_id = survey_id;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getSurvey_question() {
        return survey_question;
    }

    public void setSurvey_question(String survey_question) {
        this.survey_question = survey_question;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSurvey_option() {
        return survey_option;
    }

    public void setSurvey_option(String survey_option) {
        this.survey_option = survey_option;
    }
}
