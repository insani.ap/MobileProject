package com.project.nex.presenter.Home.Assignment;

import android.content.Context;

import androidx.room.Room;

import com.project.nex.model.localdb.database.NexDatabase;
import com.project.nex.model.localdb.pojo.RoutePlanCustomer;
import com.project.nex.util.ProgressBarHelper;
import com.project.nex.view.Home.Assignment.AssignmentView;

import java.util.List;

public class AssignmentPresenter {
    private final AssignmentView assignmentView;
    private final Context context;
    private String DATE;
    private ProgressBarHelper progressBarHelper;

    public AssignmentPresenter (AssignmentView assignmentView, Context context) {
        this.assignmentView = assignmentView;
        this.context = context;
        progressBarHelper = new ProgressBarHelper(context);
    }

    public void initCustomer() {
        try {
            NexDatabase nexDatabase = Room.databaseBuilder(context, NexDatabase.class, "dbNex").allowMainThreadQueries().build();
            List<RoutePlanCustomer> routePlanCustomers = nexDatabase.routePlanCustomerDAO().getRoutePlanCustomer(nexDatabase.todayDateTimeDAO().getDate());
            assignmentView.getDataSuccess(routePlanCustomers);
        } catch (Exception e) {
            assignmentView.getDataError();
        }
        progressBarHelper.end();
    }
}
