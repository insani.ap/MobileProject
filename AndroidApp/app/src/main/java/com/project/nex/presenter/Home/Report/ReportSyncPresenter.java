package com.project.nex.presenter.Home.Report;

import android.content.Context;

import androidx.room.Room;

import com.project.nex.model.localdb.database.NexDatabase;
import com.project.nex.model.repository.Survey.SurveyAnswerRepository;
import com.project.nex.model.repository.Survey.SurveyAnswerRepositoryCallback;
import com.project.nex.model.repository.Tagging.TaggingRepository;
import com.project.nex.model.repository.Tagging.TaggingRepositoryCallback;
import com.project.nex.view.Home.Report.ReportSyncView;

public class ReportSyncPresenter {
    private final ReportSyncView reportSyncView;
    private final Context context;

    public ReportSyncPresenter(ReportSyncView reportSyncView, Context context) {
        this.reportSyncView = reportSyncView;
        this.context = context;
    }

    public void syncData() {
        NexDatabase nexDatabase = Room.databaseBuilder(context, NexDatabase.class, "dbNex").allowMainThreadQueries().build();
        if (nexDatabase.userDAO().getUser().getRole().equalsIgnoreCase("Survey")) {
            SurveyAnswerRepository surveyAnswerRepository = new SurveyAnswerRepository(context);
            surveyAnswerRepository.syncToApi(new SurveyAnswerRepositoryCallback() {
                @Override
                public void onSuccess() {
                    reportSyncView.syncSuccess("OK");
                }

                @Override
                public void onError(String message) {
                    reportSyncView.syncFailed(message);
                }
            });
        }
        else {
            TaggingRepository taggingRepository = new TaggingRepository(context);
            taggingRepository.syncToApi(new TaggingRepositoryCallback() {
                @Override
                public void onSuccess() {
                    reportSyncView.syncSuccess("OK");
                }

                @Override
                public void onError(String message) {
                    reportSyncView.syncFailed(message);
                }
            });
        }
    }
}
