package com.project.nex.presenter.Home.Dashboard;

import android.content.Context;

import com.project.nex.model.localdb.entity.Customer;
import com.project.nex.model.localdb.entity.RoutePlan;
import com.project.nex.model.localdb.entity.Survey;
import com.project.nex.model.localdb.entity.User;
import com.project.nex.model.repository.Customer.CustomerRepository;
import com.project.nex.model.repository.Customer.CustomerRepositoryCallback;
import com.project.nex.model.repository.RoutePlan.RoutePlanRepository;
import com.project.nex.model.repository.RoutePlan.RoutePlanRepositoryCallback;
import com.project.nex.model.repository.Survey.SurveyRepository;
import com.project.nex.model.repository.Survey.SurveyRepositoryCallback;
import com.project.nex.model.repository.User.UserRepository;
import com.project.nex.util.ProgressBarHelper;
import com.project.nex.view.Home.Dashboard.DashboardView;

import java.util.List;

public class DashboardPresenter {
    private final DashboardView dashboardView;
    private UserRepository userRepository;
    private CustomerRepository customerRepository;
    private RoutePlanRepository routePlanRepository;
    private SurveyRepository surveyRepository;
    private final Context context;
    private final ProgressBarHelper progressBarHelper;
    private String status = "";

    public DashboardPresenter(DashboardView dashboardView, Context context) {
        this.dashboardView = dashboardView;
        this.context = context;
        progressBarHelper = new ProgressBarHelper(context);
    }

    public void syncCard(String statusFrom) {
        progressBarHelper.start();
        if (null == statusFrom) {
            syncOk();
        }
        else {
            customerRepository = new CustomerRepository(context);
            customerRepository.getCustomer(new CustomerRepositoryCallback() {
                @Override
                public void onSuccess(List<Customer> data) {
                    status = customerRepository.saveCustomer(data);
                    if (status.equalsIgnoreCase("Something Error")) {
                        syncError(status);
                    }
                    else {
                        syncRoutePlan();
                    }
                }
                @Override
                public void onError() {
                    syncError("Server Error");
                }
            });
        }
    }

    public void syncRoutePlan() {
        routePlanRepository = new RoutePlanRepository(context);
        routePlanRepository.getRoutePlan(new RoutePlanRepositoryCallback() {
            @Override
            public void onSuccess(List<RoutePlan> routePlan) {
                status = routePlanRepository.saveRoutePlan(routePlan);
                if (status.equalsIgnoreCase("Something Error")) {
                    syncError(status);
                }
                else {
                    userRepository = new UserRepository(context);
                    if (userRepository.getLocalUser().getRole().equalsIgnoreCase("Survey")) {
                        syncSurvey();
                    }
                    else {
                        syncOk();
                    }
                }
            }

            @Override
            public void onError() {
                syncError("Server Error");
            }
        });
    }

    public void syncSurvey() {
        surveyRepository = new SurveyRepository(context);
        surveyRepository.getSurvey(new SurveyRepositoryCallback() {
            @Override
            public void onSuccess(List<Survey> surveys) {
                status = surveyRepository.saveSurvey(surveys);
                if (status.equalsIgnoreCase("Something Error")) {
                    syncError(status);
                }
                else {
                    syncOk();
                }
            }

            @Override
            public void onError() {
                syncError("Server Error");
            }
        });
    }

    public void syncOk() {
        getLocalDb();
    }

    public void syncError(String status) {
        getLocalDb();
        dashboardView.errorSync(status);
    }

    public void getLocalDb() {
        RoutePlanRepository routePlanRepository = new RoutePlanRepository(context);
        UserRepository userRepository = new UserRepository(context);
        User user = userRepository.getLocalUser();
        dashboardView.normalCard(routePlanRepository.RouteToday(), routePlanRepository.RouteTodayMarked("Finished"), routePlanRepository.RouteTodayMarked("Synced"), status);
        dashboardView.dashboardAdditionalInfo(user.getName(), user.getId(), user.getRole(), user.getUrlPhoto());
        progressBarHelper.end();
    }
}