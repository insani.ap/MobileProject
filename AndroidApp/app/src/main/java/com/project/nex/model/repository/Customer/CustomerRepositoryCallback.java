package com.project.nex.model.repository.Customer;

import com.project.nex.model.localdb.entity.Customer;

import java.util.List;

public interface CustomerRepositoryCallback {
    void onSuccess(List<Customer> data);
    void onError();
}