package com.project.nex.view.Logout;

public interface LogoutView {
    void logoutSuccess();
    void logoutFailed();
}