package com.project.nex.presenter.Home.Data.Customer;

import android.content.Context;

import com.project.nex.model.localdb.entity.Customer;
import com.project.nex.model.repository.Customer.CustomerRepository;
import com.project.nex.view.Home.Data.Customer.DataListCustomerView;

import java.util.ArrayList;
import java.util.List;

public class DataListPresenter {
    private DataListCustomerView dataListCustomerView;
    private CustomerRepository customerRepository;
    private List<Customer> customerList;
    private List<String> name;
    private List<Integer> id;

    public DataListPresenter(DataListCustomerView dataListCustomerView) { this.dataListCustomerView = dataListCustomerView; }

    public void syncDataFromLocalDb(Context context) {
        try {
            customerRepository = new CustomerRepository(context);
            customerList = new ArrayList<Customer>();
            name = new ArrayList<String>();
            id = new ArrayList<Integer>();
            customerList = customerRepository.getAllLocalCustomer();
            for (Customer customer : customerList) {
                name.add(customer.getName());
                id.add(customer.getId());
            }
            dataListCustomerView.getDataSuccess(name, id);
        } catch (Exception e) {
            dataListCustomerView.getDataError();
        }
    }
}
