package com.project.nex.model.repository.RoutePlan;

import com.project.nex.model.localdb.entity.RoutePlan;

import java.util.List;

public interface RoutePlanRepositoryCallback {
    void onSuccess(List<RoutePlan> routePlan);
    void onError();
}
