package com.project.nex.model.localdb.dao;

import androidx.room.Dao;
import androidx.room.Query;

import com.project.nex.model.localdb.pojo.RoutePlanCustomer;
import com.project.nex.model.localdb.pojo.RoutePlanNameAddress;

import java.util.List;

@Dao
public interface RoutePlanCustomerDAO {
    @Query("SELECT route_plan.id, customer.name, route_plan.status " +
            "FROM route_plan " +
            "INNER JOIN customer ON route_plan.customer_id = customer.id " +
            "WHERE route_plan.date=:date")
    List<RoutePlanCustomer> getRoutePlanCustomer(String date);

    @Query("SELECT route_plan.id, customer.name, route_plan.status " +
            "FROM route_plan " +
            "INNER JOIN customer ON route_plan.customer_id = customer.id " +
            "WHERE route_plan.date=:date AND route_plan.status!='Not Finished'")
    List<RoutePlanCustomer> getRoutePlanCustomerForReport(String date);

    @Query("SELECT customer.name, customer.address " +
            "FROM route_plan " +
            "LEFT JOIN customer ON customer.id = route_plan.customer_id " +
            "WHERE route_plan.date=:date")
    List<RoutePlanNameAddress> getNameAddressCustomerRoutePlan(String date);
}
