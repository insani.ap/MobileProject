package com.project.nex.presenter.Home.Assignment.Survey;

import android.content.Context;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.project.nex.R;
import com.project.nex.model.localdb.entity.Survey;
import com.project.nex.model.repository.Survey.SurveyRepository;
import com.project.nex.view.Home.Assignment.Survey.SurveyView;

import java.util.List;

public class SurveyPresenter {
    public final static int ID_FREE_TEXT = 100;
    public final static int ID_RADIO_BUTTON = 300;
    public final static int ID_CHECK_BOX = 500;
    public final static int ID_BUTTON = 999;
    private SurveyView surveyView;
    private Context context;
    private LinearLayout.LayoutParams checkBoxConfig;
    private LinearLayout.LayoutParams buttonConfig;

    public SurveyPresenter(SurveyView surveyView, Context context) {
        this.surveyView = surveyView;
        this.context = context;
        config();
    }

    public void initLayout() {
        try {
            int question = 1;
            int markRad = 0;
            int markCheck = 0;
            LinearLayout linearLayoutMaster = new LinearLayout(context);
            linearLayoutMaster.setOrientation(LinearLayout.VERTICAL);
            SurveyRepository surveyRepository = new SurveyRepository(context);
            List<Survey> surveyList = surveyRepository.getAllLocalSurvey();
            for (Survey survey: surveyList) {
                if (survey.getType().equalsIgnoreCase("FreeText")) {
                    linearLayoutMaster.addView(createFreeTextLayout(survey, question));
                    question++;
                }
                else if (survey.getType().equalsIgnoreCase("RadioButton")) {
                    linearLayoutMaster.addView(createRadioButtonLayout(survey, markRad, question));
                    markRad = survey.getSurveyOption().split(";").length;
                    question++;
                }
                else if (survey.getType().equalsIgnoreCase("CheckBox")) {
                    linearLayoutMaster.addView(createCheckBoxLayout(survey, markCheck, question));
                    markCheck = survey.getSurveyOption().split(";").length;
                    question++;
                }
            }
            Button button = new Button(context);
            button.setText("Save");
            button.setTextSize(16);
            button.setId(ID_BUTTON);
            button.setLayoutParams(buttonConfig);
            linearLayoutMaster.addView(button);
            surveyView.createLayoutSuccess(linearLayoutMaster);
        } catch (Exception e) {
            surveyView.createLayoutError();
        }
    }


    public LinearLayout createFreeTextLayout(Survey survey, int question) {
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(LinearLayout.VERTICAL);

        TextView textViewQuestion = new TextView(context);
        EditText editTextAnswer = new EditText(context);

        textViewQuestion.setText("\n"+question+". "+survey.getSurveyQuestion());
        textViewQuestion.setPadding(12,0,0,0);
        textViewQuestion.setTextSize(18);
        textViewQuestion.setTextColor(context.getResources().getColor(R.color.black));

        editTextAnswer.setHint("Your Answer");
        editTextAnswer.setId(survey.getId()+ID_FREE_TEXT);

        linearLayout.addView(textViewQuestion);
        linearLayout.addView(editTextAnswer);

        return linearLayout;
    }

    public LinearLayout createRadioButtonLayout(Survey survey, int mark, int question) {
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(LinearLayout.VERTICAL);

        TextView textViewQuestion = new TextView(context);

        textViewQuestion.setText("\n"+question+". "+survey.getSurveyQuestion());
        textViewQuestion.setPadding(12,0,0,0);
        textViewQuestion.setTextSize(18);
        textViewQuestion.setTextColor(context.getResources().getColor(R.color.black));

        linearLayout.addView(textViewQuestion);

        RadioGroup radioGroup = new RadioGroup(context);
        radioGroup.setOrientation(RadioGroup.VERTICAL);

        String[] option = survey.getSurveyOption().split(";");
        RadioButton[] radioButton = new RadioButton[option.length];

        for (int i = 0; i < option.length ; i++) {
            radioButton[i] = new RadioButton(context);
            radioButton[i].setText(option[i]);
            radioButton[i].setTextColor(context.getResources().getColor(R.color.black));
            radioButton[i].setId(ID_RADIO_BUTTON+mark);
            radioButton[i].setAllCaps(true);
            radioGroup.addView(radioButton[i]);
            mark++;
        }

        linearLayout.addView(radioGroup);
        return linearLayout;
    }

    public LinearLayout createCheckBoxLayout(Survey survey, int mark, int question) {
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(LinearLayout.VERTICAL);

        TextView textViewQuestion = new TextView(context);

        textViewQuestion.setText("\n"+question+". "+survey.getSurveyQuestion());
        textViewQuestion.setPadding(12,0,0,0);
        textViewQuestion.setTextSize(18);
        textViewQuestion.setTextColor(context.getResources().getColor(R.color.black));

        linearLayout.addView(textViewQuestion);

        String[] option = survey.getSurveyOption().split(";");
        CheckBox[] checkBoxes = new CheckBox[option.length];

        for (int i = 0; i < option.length ; i++) {
            checkBoxes[i] = new CheckBox(context);
            checkBoxes[i].setText(option[i]);
            checkBoxes[i].setTextColor(context.getResources().getColor(R.color.black));
            checkBoxes[i].setId(ID_CHECK_BOX+mark);
            checkBoxes[i].setLayoutParams(checkBoxConfig);
            linearLayout.addView(checkBoxes[i]);
            mark++;
        }
        return linearLayout;
    }

    public void config() {
        checkBoxConfig = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        buttonConfig = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        buttonConfig.setMargins(0,0,0,64);
    }
}
