package com.project.nex.model.repository.User;

import android.content.Context;

import androidx.room.Room;

import com.project.nex.model.localdb.database.NexDatabase;
import com.project.nex.model.localdb.entity.User;
import com.project.nex.model.repository.Retrofit.Config.MasterApi;
import com.project.nex.model.repository.Retrofit.Config.RetrofitConf;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserRepository {
    private final NexDatabase nexDatabase;
    private final MasterApi masterApi;

    public UserRepository(Context context) {
        nexDatabase = Room.databaseBuilder(context, NexDatabase.class, "dbNex").allowMainThreadQueries().build();
        masterApi = RetrofitConf.init().create(MasterApi.class);
    }

    public void checkUser(String username, String password, UserRepositoryCallback userRepositoryCallback) {
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        Call<User> call = masterApi.getUser(user);

        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.body() != null ) {
                    userRepositoryCallback.onSuccess(response.body());
                }
            }
            @Override
            public void onFailure(Call<User> call, Throwable t) {
                userRepositoryCallback.onError("Not Match");
            }
        });
    }

    public String saveUser(User user){
        try {
            nexDatabase.userDAO().insertUser(user);
            return "OK";
        } catch (Exception e) {
            return "Error";
        }
    }

    public User getLocalUser() {
        return nexDatabase.userDAO().getUser();
    }
}
