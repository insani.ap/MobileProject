package com.project.nex.view.Home.Report.ReportDetails;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.project.nex.R;
import com.project.nex.model.localdb.entity.Tagging;
import com.project.nex.presenter.Home.Report.ReportAssignmentPresenter;
import com.project.nex.util.ImageHelper;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class ReportActivity extends AppCompatActivity implements ReportAssignmentView {
    private final int READ_STORAGE_PERMISSION = 8;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, READ_STORAGE_PERMISSION);
        }
        else {
            getData(getIntent().getStringExtra("id"));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == READ_STORAGE_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent takePicture = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                getData(getIntent().getStringExtra("id"));
            }
        }
    }

    public void getData(String id) {
        ReportAssignmentPresenter reportAssignmentPresenter = new ReportAssignmentPresenter(this, this);
        reportAssignmentPresenter.getDetails(id);
    }

    @Override
    public void getDataSuccessTagging(Tagging tagging) {
        setContentView(R.layout.activity_tagging);
        int year = Integer.parseInt(tagging.getTglNonaktifAsset().substring(0,4));
        int month = Integer.parseInt(tagging.getTglNonaktifAsset().substring(5,7));
        int day = Integer.parseInt(tagging.getTglNonaktifAsset().substring(8,10));
        DateFormat dateFormat = new SimpleDateFormat("EEEE, dd MMMM yyyy");
        Date date = new Date(year-1900, month-1, day);


        TextView textViewHeader = findViewById(R.id.tagging_header);
        TextView textViewImageName = findViewById(R.id.tagging_photo_name);
        EditText editTextSerialNumber = findViewById(R.id.tagging_serial_number);
        EditText editTextContractNumber = findViewById(R.id.tagging_contract_number);
        EditText editTextDate = findViewById(R.id.tagging_date);
        ImageView imageViewPhoto = findViewById(R.id.tagging_image);

        Button buttonTake = findViewById(R.id.tagging_button_take_photo);
        Button buttonSave = findViewById(R.id.tagging_save);

        buttonSave.setVisibility(View.GONE);
        buttonTake.setVisibility(View.GONE);

        textViewHeader.setText("TAGGING DETAILS");
        textViewImageName.setText(tagging.getUrlPhoto());
        editTextContractNumber.setText(String.valueOf(tagging.getNomerKontrak()));
        editTextDate.setText(dateFormat.format(date));

        editTextSerialNumber.setText(tagging.getSerialNumber());
        editTextSerialNumber.setInputType(InputType.TYPE_NULL);
        editTextContractNumber.setInputType(InputType.TYPE_NULL);
        editTextDate.setInputType(InputType.TYPE_NULL);

        imageViewPhoto.setImageBitmap(ImageHelper.getImage(tagging.getUrlPhoto()));
        imageViewPhoto.setClickable(true);
        imageViewPhoto.setOnClickListener(v -> openImage(tagging.getUrlPhoto()));
    }

    @Override
    public void getDataError() {
        Toast.makeText(this, "DATA INVALID", Toast.LENGTH_LONG).show();
    }

    @Override
    public void getDataSuccesSurvey(LinearLayout linearLayout) {
        setContentView(R.layout.activity_survey);
        TextView textView = findViewById(R.id.survey_header);
        textView.setText("SURVEY DETAILS");
        LinearLayout linearLayoutMaster = findViewById(R.id.master_survey);
        linearLayoutMaster.addView(linearLayout);
    }

    public void openImage(String path) {
        Intent intent = new Intent(this, TaggingImagePreviewActivity.class);
        intent.putExtra("path", path);
        startActivity(intent);
    }
}