package com.project.nex.model.repository.Survey;

import com.project.nex.model.localdb.entity.Survey;

import java.util.List;

public interface SurveyRepositoryCallback {
    void onSuccess(List<Survey> surveys);
    void onError();
}
