package com.project.nex.view.Home.Assignment.Survey;

import android.widget.LinearLayout;

public interface SurveyView {
    void createLayoutSuccess(LinearLayout linearLayout);
    void createLayoutError();
}
