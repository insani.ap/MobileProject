package com.project.nex.model.repository.Survey;

import android.content.Context;

import androidx.room.Room;

import com.project.nex.model.localdb.database.NexDatabase;
import com.project.nex.model.localdb.entity.SurveyAnswer;
import com.project.nex.model.repository.Retrofit.Config.MasterApi;
import com.project.nex.model.repository.Retrofit.Config.RetrofitConf;
import com.project.nex.model.repository.Retrofit.pojo.SurveyAnswerRetrofit;
import com.project.nex.model.repository.RoutePlan.RoutePlanRepository;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SurveyAnswerRepository {
    private final NexDatabase nexDatabase;
    private final MasterApi masterApi;
    private final Context context;
    private String date;

    public SurveyAnswerRepository(Context context) {
        nexDatabase = Room.databaseBuilder(context, NexDatabase.class, "dbNex").allowMainThreadQueries().build();
        masterApi = RetrofitConf.init().create(MasterApi.class);
        this.context = context;
        date = nexDatabase.todayDateTimeDAO().getDate();
    }

    public String saveAnswer(List <SurveyAnswer> surveyAnswers, int idRoutePlan) {
        try {
            nexDatabase.surveyAnswerDAO().insertAllSurveyAnswer(surveyAnswers);
            nexDatabase.routePlanDAO().updateStatusRoutePlan("Finished", idRoutePlan);
            return "OK";
        } catch (Exception e) {
            return "Error";
        }
    }

    public void syncToApi(SurveyAnswerRepositoryCallback surveyAnswerRepositoryCallback) {
        List<SurveyAnswer> surveyAnswerList = new ArrayList<>();
        List<SurveyAnswerRetrofit> surveyAnswerRetrofitList = new ArrayList<>();
        List<Integer> idRoutePlanFinished = nexDatabase.routePlanDAO().getAllRoutePlanIdFinished(date);

        if (idRoutePlanFinished.size() != 0) {
            for (int i = 0; i < idRoutePlanFinished.size(); i++) {
                surveyAnswerList.addAll(nexDatabase.surveyAnswerDAO().getSurveyAnswer(idRoutePlanFinished.get(i)));
            }

            for (SurveyAnswer surveyAnswer : surveyAnswerList) {
                surveyAnswerRetrofitList.add(
                    new SurveyAnswerRetrofit(
                        0,
                        surveyAnswer.getAnswer(),
                        nexDatabase.todayDateTimeDAO().getDate(),
                        nexDatabase.surveyDAO().getDetailsSurvey(surveyAnswer.getSurveyId()),
                        nexDatabase.customerDAO().getDetailsCustomer(nexDatabase.routePlanDAO().getCustomerIdFromRoutePlan(surveyAnswer.getRoutePlanId()))));
            }
            Call<String> call = masterApi.saveRoutePlanSurvey(surveyAnswerRetrofitList);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {
                    if (response.body().equalsIgnoreCase("true")) {
                        RoutePlanRepository routePlanRepository = new RoutePlanRepository(context);
                        for (int i = 0; i < idRoutePlanFinished.size(); i++) {
                            routePlanRepository.updateStatusSynced(idRoutePlanFinished.get(i));
                            routePlanRepository.updateRoutePlanStatus(idRoutePlanFinished.get(i));
                        }
                        surveyAnswerRepositoryCallback.onSuccess();
                    }
                    else {
                        surveyAnswerRepositoryCallback.onError("Error");
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    surveyAnswerRepositoryCallback.onError("Error");
                }
            });
        }
        else {
            surveyAnswerRepositoryCallback.onError("No Need For Sync Data");
        }
    }
}
