package com.project.nex.view.Home.Data.Customer.Adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.project.nex.R;
import com.project.nex.view.Home.Data.Customer.Details.CustomerDetailsActivity;

import java.util.List;

public class CardCustomerAdapter extends RecyclerView.Adapter<CardCustomerAdapter.ViewHolder> {
    //View
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView textViewName;
        private TextView textViewId;

        public ViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            this.textViewName = view.findViewById(R.id.card_lc_customer_name);
            this.textViewId = view.findViewById(R.id.card_lc_customer_id);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(view.getContext(), CustomerDetailsActivity.class);
            intent.putExtra("name", this.textViewName.getText().toString());
            intent.putExtra("id", this.textViewId.getText().toString());
            view.getContext().startActivity(intent);
        }
    }

    private List<String> name;
    private List<Integer> id;

    //Constructor
    public CardCustomerAdapter(List<String> name, List<Integer> id){
        this.name = name;
        this.id = id;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public CardCustomerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rowItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_lc, parent, false);
        return new ViewHolder(rowItem);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(CardCustomerAdapter.ViewHolder holder, int position) {
        holder.textViewName.setText(this.name.get(position));
        holder.textViewId.setText(this.id.get(position).toString());
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return this.name.size();
    }
}