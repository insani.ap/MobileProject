package com.project.nex.view.Home.Assignment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.project.nex.R;
import com.project.nex.model.localdb.database.NexDatabase;
import com.project.nex.model.localdb.pojo.RoutePlanCustomer;
import com.project.nex.presenter.Home.Assignment.AssignmentPresenter;
import com.project.nex.view.Home.AdapterDashboardAndReport.CardCustomerThreeColorAdapter;

import java.util.List;

public class AssignmentFragment extends Fragment implements AssignmentView {
    private TextView textViewNoData;
    private TextView textViewHeader;
    private RecyclerView recyclerView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_assignment, container, false);
        NexDatabase nexDatabase = Room.databaseBuilder(getActivity(), NexDatabase.class, "dbNex").allowMainThreadQueries().build();
        textViewHeader = view.findViewById(R.id.assignment_header_menu);
        if (nexDatabase.userDAO().findUserRole().equalsIgnoreCase("Survey")) {
            textViewHeader.setText("SURVEY MENU");
        }
        else {
            textViewHeader.setText("TAGGING MENU");
        }

        textViewNoData = view.findViewById(R.id.assignment_no_data);
        recyclerView = view.findViewById(R.id.assignment_card);
        initCustomer();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        initCustomer();
    }

    private void initCustomer() {
        AssignmentPresenter assignmentPresenter = new AssignmentPresenter(this, getActivity());
        assignmentPresenter.initCustomer();
    }

    @Override
    public void getDataSuccess(List<RoutePlanCustomer> routePlanCustomers) {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(new CardCustomerThreeColorAdapter(getActivity(), routePlanCustomers, "From Assignment", textViewHeader.getText().toString()));
    }

    @Override
    public void getDataError() {
        textViewNoData.setText("NO DATA FOUND!!!");
    }
}
