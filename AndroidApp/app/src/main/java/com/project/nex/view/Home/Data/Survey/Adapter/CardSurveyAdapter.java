package com.project.nex.view.Home.Data.Survey.Adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.project.nex.R;
import com.project.nex.view.Home.Data.Survey.Details.SurveyDetailsActivity;

import java.util.List;

public class CardSurveyAdapter extends RecyclerView.Adapter<CardSurveyAdapter.ViewHolder> {
    //View
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView textViewId;
        private TextView textViewQuestion;

        public ViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            this.textViewId = view.findViewById(R.id.card_lc_customer_id);
            this.textViewQuestion = view.findViewById(R.id.card_lc_customer_name);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(view.getContext(), SurveyDetailsActivity.class);
            intent.putExtra("id", this.textViewId.getText().toString());
            view.getContext().startActivity(intent);
        }
    }

    private List<Integer> id;
    private List<String> question;

    //Constructor
    public CardSurveyAdapter(List<String> question, List<Integer> id){
        this.question = question;
        this.id = id;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public CardSurveyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rowItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_lc, parent, false);
        return new ViewHolder(rowItem);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(CardSurveyAdapter.ViewHolder holder, int position) {
        holder.textViewId.setText(this.id.get(position).toString());
        holder.textViewQuestion.setText(this.question.get(position));
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return this.id.size();
    }
}