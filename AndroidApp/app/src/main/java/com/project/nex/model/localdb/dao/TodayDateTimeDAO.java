package com.project.nex.model.localdb.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.project.nex.model.localdb.entity.TodayDateTime;
@Dao
public interface TodayDateTimeDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertDateTime(TodayDateTime todayDateTime);

    @Query("SELECT date FROM today_date_time")
    String getDate();

    @Query("SELECT time FROM today_date_time")
    Long getTime();
}
