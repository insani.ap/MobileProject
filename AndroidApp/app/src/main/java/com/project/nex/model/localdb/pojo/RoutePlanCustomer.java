package com.project.nex.model.localdb.pojo;

public class RoutePlanCustomer {
    private int id;
    private String name;
    private String status;

    public RoutePlanCustomer(int id, String name, String status) {
        this.id = id;
        this.name = name;
        this.status = status;
    }

    public RoutePlanCustomer() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
