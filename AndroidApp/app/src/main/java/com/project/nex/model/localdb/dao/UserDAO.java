package com.project.nex.model.localdb.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.project.nex.model.localdb.entity.User;

@Dao
public interface UserDAO {
    @Query("SELECT * FROM user")
    User getUser();

    @Query("DELETE FROM user")
    void deleteUser();

    @Query("SELECT role from user")
    String findUserRole();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertUser(User user);
}