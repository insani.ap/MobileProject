package com.project.nex.view.Home.Data;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.room.Room;

import com.project.nex.R;
import com.project.nex.model.localdb.database.NexDatabase;
import com.project.nex.presenter.Home.Data.DataPresenter;
import com.project.nex.view.Home.Data.Customer.ListCustomerActivity;
import com.project.nex.view.Home.Data.RoutePlan.ListRoutePlanActivity;
import com.project.nex.view.Home.Data.Survey.ListSurveyActivity;

import static android.widget.Toast.LENGTH_SHORT;

public class DataFragment extends Fragment implements DataView {
    private CardView cardViewCustomer;
    private CardView cardViewRoute;
    private CardView cardViewSurvey;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_data, container, false);
        init(view);
        Button buttonSync = view.findViewById(R.id.download_sync);
        cardViewCustomer = view.findViewById(R.id.download_card_customer);
        cardViewSurvey = view.findViewById(R.id.download_card_survey);
        cardViewRoute = view.findViewById(R.id.download_card_route_plan);

        buttonSync.setOnClickListener(v -> syncCheck(getActivity()));

        cardViewCustomer.setOnClickListener(v -> listCustomer());
        cardViewSurvey.setOnClickListener(v -> listSurvey());
        cardViewRoute.setOnClickListener(v -> listRoute());

        return view;
    }

    public void init(View view) {
        cardViewSurvey = view.findViewById(R.id.download_card_survey);
        cardViewRoute = view.findViewById(R.id.download_card_route_plan);
        NexDatabase nexDatabase = Room.databaseBuilder(getActivity(), NexDatabase.class, "dbNex").allowMainThreadQueries().build();
        if (nexDatabase.userDAO().findUserRole().equalsIgnoreCase("Survey")) {
            this.cardViewRoute.setVisibility(View.GONE);
        }
        else {
            this.cardViewSurvey.setVisibility(View.GONE);
        }
    }

    public void listCustomer() {
        Intent intent = new Intent(getActivity(), ListCustomerActivity.class);
        startActivity(intent);
    }

    public void listRoute() {
        Intent intent = new Intent(getActivity(), ListRoutePlanActivity.class);
        startActivity(intent);
    }

    public void listSurvey() {
        Intent intent = new Intent(getActivity(), ListSurveyActivity.class);
        startActivity(intent);
    }

    public void syncCheck(Context context) {
        DataPresenter dataPresenter = new DataPresenter(this, getActivity());
        NexDatabase nexDatabase = Room.databaseBuilder(getActivity(), NexDatabase.class, "dbNex").allowMainThreadQueries().build();
        dataPresenter.syncData();
    }

    @Override
    public void syncDataSuccess(String message) {
        if (message.equalsIgnoreCase("Sync Success")) {
            Toast.makeText(getActivity(), "Sync Success", LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(getActivity(), "Already Up to Date", LENGTH_SHORT).show();
        }
    }

    @Override
    public void syncDataError(String message) {
        Toast.makeText(getActivity(), "Sync Error", LENGTH_SHORT).show();
    }
}
