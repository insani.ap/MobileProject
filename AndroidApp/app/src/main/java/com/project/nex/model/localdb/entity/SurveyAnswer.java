package com.project.nex.model.localdb.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "survey_answer")
public class SurveyAnswer {
    @ColumnInfo(name = "id") @PrimaryKey(autoGenerate = true) int id;
    @ColumnInfo(name = "answer") String answer;
    @ColumnInfo(name = "route_plan_id") int routePlanId;
    @ColumnInfo(name = "survey_id") int surveyId;

    public SurveyAnswer() {}

    public SurveyAnswer(int id, String answer, int routePlanId, int surveyId) {
        this.id = id;
        this.answer = answer;
        this.routePlanId = routePlanId;
        this.surveyId = surveyId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public int getRoutePlanId() {
        return routePlanId;
    }

    public void setRoutePlanId(int routePlanId) {
        this.routePlanId = routePlanId;
    }

    public int getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(int surveyId) {
        this.surveyId = surveyId;
    }
}