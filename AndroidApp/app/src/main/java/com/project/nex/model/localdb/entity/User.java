package com.project.nex.model.localdb.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "user")
public class User {
    @PrimaryKey @ColumnInfo(name = "id") int id;
    @ColumnInfo(name = "username") String username;
    @ColumnInfo(name = "email") String email;
    @ColumnInfo(name = "name") String name;
    @ColumnInfo(name = "password") String password;
    @ColumnInfo(name = "gender") String gender;
    @ColumnInfo(name = "phone_number") String phoneNumber;
    @ColumnInfo(name = "role") String role;
    @ColumnInfo(name = "url_photo") String urlPhoto;

    public User(int id, String username, String email, String name, String password, String gender, String phoneNumber, String role, String urlPhoto) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.name = name;
        this.password = password;
        this.gender = gender;
        this.phoneNumber = phoneNumber;
        this.role = role;
        this.urlPhoto = urlPhoto;
    }

    public User() {

    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getRole() {
        return role;
    }
    public void setRole(String role) {
        this.role = role;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getName() { return name; }
    public void setName(String name) { this.name = name; }
    public String getGender() {
        return gender;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }
    public String getPhoneNumber() {
        return phoneNumber;
    }
    public void setPhoneNumber(String phoneNumber) { this.phoneNumber = phoneNumber; }

    public String getUrlPhoto() {
        return urlPhoto;
    }

    public void setUrlPhoto(String urlPhoto) {
        this.urlPhoto = urlPhoto;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", gender='" + gender + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", role='" + role + '\'' +
                ", urlPhoto='" + urlPhoto + '\'' +
                '}';
    }
}
