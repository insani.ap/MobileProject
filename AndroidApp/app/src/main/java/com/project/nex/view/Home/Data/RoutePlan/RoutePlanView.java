package com.project.nex.view.Home.Data.RoutePlan;

import com.project.nex.model.localdb.pojo.RoutePlanNameAddress;

import java.util.List;

public interface RoutePlanView {
    void getDataSuccess(List<RoutePlanNameAddress> routePlanNameAddresses);
    void getDataError();
}
