package com.project.nex.view.Home.Report.ReportDetails;

import android.widget.LinearLayout;

import com.project.nex.model.localdb.entity.Tagging;

public interface ReportAssignmentView {
    void getDataSuccessTagging(Tagging tagging);
    void getDataError();
    void getDataSuccesSurvey(LinearLayout linearLayout);
}
