package com.project.nex.view.Home.Report.ReportDetails;

import android.os.Bundle;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.project.nex.R;
import com.project.nex.util.ImageHelper;

public class TaggingImagePreviewActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview_image);
        ImageView imageView = findViewById(R.id.image_large);
        imageView.setImageBitmap(ImageHelper.getImage(getIntent().getStringExtra("path")));
    }
}
