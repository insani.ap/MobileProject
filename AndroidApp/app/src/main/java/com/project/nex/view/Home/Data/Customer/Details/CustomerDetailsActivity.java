package com.project.nex.view.Home.Data.Customer.Details;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.project.nex.R;
import com.project.nex.model.localdb.entity.Customer;
import com.project.nex.presenter.Home.Data.Customer.Details.CustomerDetailsPresenter;

public class CustomerDetailsActivity extends AppCompatActivity implements CustomerDetailsView {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_details);
        initDetails(getIntent().getStringExtra("id"), getIntent().getStringExtra("name"));
    }

    public void initDetails(String id, String name) {
        CustomerDetailsPresenter customerDetailsPresenter = new CustomerDetailsPresenter(this);
        customerDetailsPresenter.details(id, name, getApplicationContext());
    }

    @Override
    public void show(Customer customer) {
        TextView textViewId = findViewById(R.id.customer_id);
        TextView textViewName = findViewById(R.id.customer_name);
        TextView textViewPhone = findViewById(R.id.customer_phone);
        TextView textViewEmail = findViewById(R.id.customer_email);
        TextView textViewAddress = findViewById(R.id.customer_address);
        textViewId.setText(String.valueOf(customer.getId()));
        textViewName.setText(customer.getName());
        textViewPhone.setText(customer.getPhoneNumber());
        textViewEmail.setText(customer.getEmail());
        textViewAddress.setText(customer.getAddress());
    }
}
