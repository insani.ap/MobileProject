package com.project.nex.util;

import com.project.nex.model.repository.Retrofit.Config.MasterApi;
import com.project.nex.model.repository.Retrofit.Config.RetrofitConf;
import com.project.nex.view.LoadingScreen.LoadingCallback;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConnectionHelper {
    //Place Your API IP Here
    public static final String URL = "http://10.10.4.22:8080/";

    public static void checkServerAndDevice(LoadingCallback loadingCallback) {
        MasterApi masterApi = RetrofitConf.init().create(MasterApi.class);
        Call<String> call = masterApi.getDate();
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                loadingCallback.onSuccess();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                String[] cause = t.getMessage().split("from");
                if (cause.length == 1) {
                    loadingCallback.onError("Device");
                }
                else {
                    loadingCallback.onError("Server");
                }
            }
        });
    }

    public static void getDate(ConnectionHelperCallback connectionHelperCallback) {
        MasterApi masterApi = RetrofitConf.init().create(MasterApi.class);
        Call<String> call = masterApi.getDate();
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                connectionHelperCallback.onSuccess(response.body(), null);
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                connectionHelperCallback.onError();
            }
        });
    }

    public static void getTime(ConnectionHelperCallback connectionHelperCallback) {
        MasterApi masterApi = RetrofitConf.init().create(MasterApi.class);
        Call<Long> call = masterApi.getTime();
        call.enqueue(new Callback<Long>() {
            @Override
            public void onResponse(Call<Long> call, Response<Long> response) {
                connectionHelperCallback.onSuccess(null, response.body());
            }

            @Override
            public void onFailure(Call<Long> call, Throwable t) {
                connectionHelperCallback.onError();
            }
        });
    }
}
