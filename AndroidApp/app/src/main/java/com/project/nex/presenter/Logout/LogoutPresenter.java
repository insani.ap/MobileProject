package com.project.nex.presenter.Logout;

import android.content.Context;

import androidx.room.Room;

import com.project.nex.model.localdb.database.NexDatabase;
import com.project.nex.view.Logout.LogoutView;

public class LogoutPresenter {
    private NexDatabase nexDatabase;
    private LogoutView logoutView;

    public LogoutPresenter(LogoutView logoutView) {
        this.logoutView = logoutView;
    }

    public void logout(Context context) {
        nexDatabase = Room.databaseBuilder(context, NexDatabase.class, "dbNex").allowMainThreadQueries().build();
        try {
            nexDatabase.clearAllTables();
            logoutView.logoutSuccess();
        } catch (Exception e) {
            logoutView.logoutFailed();
        }
    }
}
