package com.project.nex.model.localdb.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "survey")
public class Survey {
    @ColumnInfo(name = "id") @PrimaryKey int id;
    @ColumnInfo(name = "survey_question") String surveyQuestion;
    @ColumnInfo(name = "type") String type;
    @ColumnInfo(name = "survey_option") String surveyOption;

    public Survey() {}

    public Survey(int id, String surveyQuestion, String type, String surveyOption) {
        this.id = id;
        this.surveyQuestion = surveyQuestion;
        this.type = type;
        this.surveyOption = surveyOption;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSurveyQuestion() {
        return surveyQuestion;
    }

    public void setSurveyQuestion(String surveyQuestion) {
        this.surveyQuestion = surveyQuestion;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSurveyOption() {
        return surveyOption;
    }

    public void setSurveyOption(String surveyOption) {
        this.surveyOption = surveyOption;
    }

    @Override
    public String toString() {
        return "Survey{" +
                "id=" + id +
                ", surveyQuestion='" + surveyQuestion + '\'' +
                ", type='" + type + '\'' +
                ", surveyOption='" + surveyOption + '\'' +
                '}';
    }
}
