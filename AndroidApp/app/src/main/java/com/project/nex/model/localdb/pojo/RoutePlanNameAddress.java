package com.project.nex.model.localdb.pojo;

public class RoutePlanNameAddress {
    String name;
    String address;

    public RoutePlanNameAddress() {}

    public RoutePlanNameAddress(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
