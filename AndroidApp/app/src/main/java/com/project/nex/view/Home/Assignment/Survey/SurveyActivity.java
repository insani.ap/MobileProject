package com.project.nex.view.Home.Assignment.Survey;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.project.nex.R;
import com.project.nex.presenter.Home.Assignment.Survey.SurveyPresenter;
import com.project.nex.presenter.Home.Assignment.Survey.SurveyProcessPresenter;

public class SurveyActivity extends AppCompatActivity implements SurveyView, SurveyProcessView {
    private LinearLayout masterLinearLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey);
        masterLinearLayout = findViewById(R.id.master_survey);
        initLayout();
        saveSurvey();
    }

    public void initLayout() {
        SurveyPresenter surveyPresenter = new SurveyPresenter(this, this);
        surveyPresenter.initLayout();
    }

    public void saveSurvey() {
        Button button = findViewById(SurveyPresenter.ID_BUTTON);
        button.setOnClickListener(v -> processSurvey());
    }

    public void processSurvey() {
        SurveyProcessPresenter surveyProcessPresenter = new SurveyProcessPresenter(this, this);
        surveyProcessPresenter.processSurvey(masterLinearLayout, Integer.parseInt(getIntent().getStringExtra("id")));
    }

    @Override
    public void createLayoutSuccess(LinearLayout linearLayout) {
        masterLinearLayout.addView(linearLayout);
    }

    @Override
    public void createLayoutError() {
        TextView textViewErr = new TextView(this);
        textViewErr.setText("Error !!!");
        textViewErr.setTextSize(28);
        textViewErr.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        textViewErr.setTextColor(getResources().getColor(R.color.red));
        masterLinearLayout.addView(textViewErr);
    }

    @Override
    public void saveOK() {
        finish();
    }

    @Override
    public void saveFailed(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
