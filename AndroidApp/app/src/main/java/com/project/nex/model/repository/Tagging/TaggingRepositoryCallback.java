package com.project.nex.model.repository.Tagging;

public interface TaggingRepositoryCallback {
    void onSuccess();
    void onError(String message);
}
