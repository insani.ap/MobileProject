package com.project.nex.presenter.Home.Data;

import android.content.Context;

import com.project.nex.model.localdb.entity.Customer;
import com.project.nex.model.localdb.entity.RoutePlan;
import com.project.nex.model.localdb.entity.Survey;
import com.project.nex.model.repository.Customer.CustomerRepository;
import com.project.nex.model.repository.Customer.CustomerRepositoryCallback;
import com.project.nex.model.repository.RoutePlan.RoutePlanRepository;
import com.project.nex.model.repository.RoutePlan.RoutePlanRepositoryCallback;
import com.project.nex.model.repository.Survey.SurveyRepository;
import com.project.nex.model.repository.Survey.SurveyRepositoryCallback;
import com.project.nex.model.repository.User.UserRepository;
import com.project.nex.util.ProgressBarHelper;
import com.project.nex.view.Home.Data.DataView;

import java.util.List;

public class DataPresenter {
    private final DataView dataView;
    private UserRepository userRepository;
    private CustomerRepository customerRepository;
    private RoutePlanRepository routePlanRepository;
    private SurveyRepository surveyRepository;
    private final Context context;
    private final ProgressBarHelper progressBarHelper;
    private String status;

    public DataPresenter(DataView dataView, Context context) {
        this.dataView = dataView;
        this.context = context;
        progressBarHelper = new ProgressBarHelper(context);
    }

    public void syncData() {
        customerRepository = new CustomerRepository(context);
        progressBarHelper.start();
        customerRepository.getCustomer(new CustomerRepositoryCallback() {
            @Override
            public void onSuccess(List<Customer> data) {
                status = customerRepository.saveCustomer(data);
                if (status.equalsIgnoreCase("Something Error")) {
                    syncError(status);
                }
                else {
                    syncRoutePlan();
                }
            }
            @Override
            public void onError() {
                syncError("Server Error");
            }
        });
    }

    public void syncRoutePlan() {
        routePlanRepository = new RoutePlanRepository(context);
        routePlanRepository.getRoutePlan(new RoutePlanRepositoryCallback() {
            @Override
            public void onSuccess(List<RoutePlan> routePlan) {
                status = routePlanRepository.saveRoutePlan(routePlan);
                if (status.equalsIgnoreCase("Something Error")) {
                    syncError(status);
                }
                else {
                    userRepository = new UserRepository(context);
                    if (userRepository.getLocalUser().getRole().equalsIgnoreCase("Survey")) {
                        syncSurvey();
                    }
                    else {
                        syncOk(status);
                    }
                }
            }

            @Override
            public void onError() {
                syncError("Server Error");
            }
        });
    }

    public void syncSurvey() {
        surveyRepository = new SurveyRepository(context);
        surveyRepository.getSurvey(new SurveyRepositoryCallback() {
            @Override
            public void onSuccess(List<Survey> surveys) {
                status = surveyRepository.saveSurvey(surveys);
                if (status.equalsIgnoreCase("Something Error")) {
                    syncError(status);
                }
                else {
                    syncOk(status);
                }
            }

            @Override
            public void onError() {
                syncError("Server Error");
            }
        });
    }

    public void syncOk(String status) {
        dataView.syncDataSuccess(status);
        progressBarHelper.end();
    }

    public void syncError(String status) {
        dataView.syncDataError(status);
        progressBarHelper.end();
    }
}
