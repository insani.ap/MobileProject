package com.project.nex.view.Home.Report;

import com.project.nex.model.localdb.pojo.RoutePlanCustomer;

import java.util.List;

public interface ReportView {
    void getDataSuccess(List<RoutePlanCustomer> routePlanCustomers);
    void getDataError();
}
