package com.project.nex.model.localdb.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "tagging")
public class Tagging {
    @ColumnInfo(name = "id") @PrimaryKey(autoGenerate = true) int id;
    @ColumnInfo(name = "serial_number") String serialNumber;
    @ColumnInfo(name = "url_photo") String urlPhoto;
    @ColumnInfo(name = "nomer_kontrak") int nomerKontrak;
    @ColumnInfo(name = "tgl_nonaktif_asset") String tglNonaktifAsset;
    @ColumnInfo(name = "tagging_date") String taggingDate;
    @ColumnInfo(name = "deleted") boolean deleted = false;
    @ColumnInfo(name = "customer_id") int customerId;
    @ColumnInfo(name = "routeplan_id") int routePlanId;

    public Tagging() {}

    public Tagging(int id, String serialNumber, String urlPhoto, int nomerKontrak, String tglNonaktifAsset, String taggingDate, boolean deleted, int customerId, int routePlanId) {
        this.id = id;
        this.serialNumber = serialNumber;
        this.urlPhoto = urlPhoto;
        this.nomerKontrak = nomerKontrak;
        this.tglNonaktifAsset = tglNonaktifAsset;
        this.taggingDate = taggingDate;
        this.deleted = deleted;
        this.customerId = customerId;
        this.routePlanId = routePlanId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getUrlPhoto() {
        return urlPhoto;
    }

    public void setUrlPhoto(String urlPhoto) {
        this.urlPhoto = urlPhoto;
    }

    public int getNomerKontrak() {
        return nomerKontrak;
    }

    public void setNomerKontrak(int nomerKontrak) {
        this.nomerKontrak = nomerKontrak;
    }

    public String getTglNonaktifAsset() {
        return tglNonaktifAsset;
    }

    public void setTglNonaktifAsset(String tglNonaktifAsset) {
        this.tglNonaktifAsset = tglNonaktifAsset;
    }

    public String getTaggingDate() {
        return taggingDate;
    }

    public void setTaggingDate(String taggingDate) {
        this.taggingDate = taggingDate;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getRoutePlanId() {
        return routePlanId;
    }

    public void setRoutePlanId(int routePlanId) {
        this.routePlanId = routePlanId;
    }
}
