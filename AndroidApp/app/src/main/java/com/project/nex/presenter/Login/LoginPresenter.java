package com.project.nex.presenter.Login;

import android.content.Context;
import android.text.TextUtils;

import com.project.nex.model.localdb.entity.User;
import com.project.nex.model.repository.DateTime;
import com.project.nex.model.repository.User.UserRepository;
import com.project.nex.model.repository.User.UserRepositoryCallback;
import com.project.nex.view.Login.LoginView;


public class LoginPresenter {
    private final LoginView loginView;
    private UserRepository userRepository;
    private String status;

    public LoginPresenter(LoginView loginView) {
        this.loginView = loginView;
    }

    public void login(String username, String password, Context context) {
        DateTime.getTodayDateTime(context);
        userRepository = new UserRepository(context);
        if(TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {
            loginView.loginError("Empty");
        }
        else {
            userRepository.checkUser(username, password, new UserRepositoryCallback() {
                @Override
                public void onSuccess(User user) {
                    status = userRepository.saveUser(user);
                    if (status.equalsIgnoreCase("OK")) {
                        loginView.loginSuccess();
                    } else {
                        loginView.loginError(status);
                    }
                }

                @Override
                public void onError(String message) {
                    loginView.loginError(message);
                }
            });
        }
    }
}