package com.project.nex.presenter.Home.Data.Survey;

import android.content.Context;

import com.project.nex.model.localdb.entity.Survey;
import com.project.nex.model.repository.Survey.SurveyRepository;
import com.project.nex.view.Home.Data.Survey.SurveyListView;

import java.util.ArrayList;
import java.util.List;

public class SurveyListPresenter {
    private SurveyListView surveyListView;
    private List<String> question;
    private List<Integer> id;
    private SurveyRepository surveyRepository;

    public SurveyListPresenter(SurveyListView surveyListView) {
        this.surveyListView = surveyListView;
    }

    public void syncDataFromLocalDb(Context context) {
        try {
            surveyRepository = new SurveyRepository(context);
            id = new ArrayList<Integer>();
            question = new ArrayList<String>();
            for (Survey survey : surveyRepository.getAllLocalSurvey()) {
                id.add(survey.getId());
                question.add("Question "+survey.getId());
            }
            surveyListView.getDataSuccess(question, id);
        } catch (Exception e) {
            surveyListView.getDataError();
        }
    }
}
