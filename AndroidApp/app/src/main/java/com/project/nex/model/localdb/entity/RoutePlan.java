package com.project.nex.model.localdb.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "route_plan")
public class RoutePlan {
    @PrimaryKey @ColumnInfo(name = "id") int id;
    @ColumnInfo(name = "date") String date;
    @ColumnInfo(name = "user_id") int userId;
    @ColumnInfo(name = "customer_id") int customerId;
    @ColumnInfo(name = "status") String status = "Not Finished";

    public RoutePlan(int id, String date, int userId, int customerId) {
        this.id = id;
        this.date = date;
        this.userId = userId;
        this.customerId = customerId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "RoutePlan{" +
                "id=" + id +
                ", date=" + date +
                ", userId=" + userId +
                ", customerId=" + customerId +
                '}';
    }
}
