package com.project.nex.presenter.Home.Report;

import android.content.Context;

import androidx.room.Room;

import com.project.nex.model.localdb.database.NexDatabase;
import com.project.nex.model.localdb.pojo.RoutePlanCustomer;
import com.project.nex.util.ProgressBarHelper;
import com.project.nex.view.Home.Report.ReportView;

import java.util.List;

public class ReportPresenter {
    private final ReportView reportView;
    private final Context context;
    private String DATE = null;
    private ProgressBarHelper progressBarHelper;

    public ReportPresenter(ReportView reportView, Context context) {
        this.reportView = reportView;
        this.context = context;
        progressBarHelper = new ProgressBarHelper(context);
    }

    public void getReportData() {
        progressBarHelper.start();
        try {
            NexDatabase nexDatabase = Room.databaseBuilder(context, NexDatabase.class, "dbNex").allowMainThreadQueries().build();
            List<RoutePlanCustomer> routePlanCustomers = nexDatabase.routePlanCustomerDAO().getRoutePlanCustomerForReport(nexDatabase.todayDateTimeDAO().getDate());
            reportView.getDataSuccess(routePlanCustomers);
        } catch (Exception e) {
            reportView.getDataError();
        }
        progressBarHelper.end();
    }
}
