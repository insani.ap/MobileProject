package com.project.nex.view.Home.Data.Survey;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.project.nex.R;
import com.project.nex.presenter.Home.Data.Survey.SurveyListPresenter;
import com.project.nex.view.Home.Data.Survey.Adapter.CardSurveyAdapter;

import java.util.List;

public class ListSurveyActivity extends AppCompatActivity implements SurveyListView {
    private SurveyListPresenter surveyListPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_survey);
        initSurvey();
    }

    public void initSurvey() {
        surveyListPresenter = new SurveyListPresenter(this);
        surveyListPresenter.syncDataFromLocalDb(this);
    }

    @Override
    public void getDataSuccess(List<String> question, List<Integer> id) {
        RecyclerView recyclerView = findViewById(R.id.list_survey_card);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new CardSurveyAdapter(question, id));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
    }

    @Override
    public void getDataError() {
        TextView textView = findViewById(R.id.list_survey_no_data);
        textView.setText("NO DATA FOUND!!!");
    }
}
