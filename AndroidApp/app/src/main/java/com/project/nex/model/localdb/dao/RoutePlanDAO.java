package com.project.nex.model.localdb.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.project.nex.model.localdb.entity.RoutePlan;

import java.util.List;

@Dao
public interface RoutePlanDAO {
    @Query("UPDATE route_plan SET status=:status WHERE id=:id")
    void updateStatusRoutePlan(String status, int id);

    @Query("DELETE FROM route_plan WHERE id=:id")
    void deleteMissMatch(String id);

    @Query("SELECT id FROM route_plan WHERE date=:date AND status='Finished'")
    List<Integer> getAllRoutePlanIdFinished(String date);

    @Query("SELECT id FROM route_plan WHERE date=:date")
    List<Integer> getAllIdByDate(String date);

    @Query("SELECT * FROM route_plan WHERE id=:id")
    RoutePlan getOneRoutePlan(String id);

    @Query("SELECT status FROM route_plan WHERE id=:id")
    String getStatus(String id);

    @Query("SELECT * FROM route_plan WHERE date=:date")
    List<RoutePlan> getRoutePlan(String date);

    @Query("SELECT COUNT(id) FROM route_plan WHERE date=:date AND status=:status")
    int getMarked(String date, String status);

    @Query("SELECT COUNT(id) FROM route_plan WHERE date=:date")
    int getMark(String date);

    @Query("SELECT customer_id FROM route_plan WHERE id=:id")
    int getCustomerIdFromRoutePlan(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertRoutePlan(RoutePlan routePlan);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllRoutePlan(List<RoutePlan> routePlans);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateRoutePlan(RoutePlan... routePlans);

}
