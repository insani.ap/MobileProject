package com.project.nex.model.repository.User;

import com.project.nex.model.localdb.entity.User;

public interface UserRepositoryCallback {
    void onSuccess(User user);
    void onError(String message);
}