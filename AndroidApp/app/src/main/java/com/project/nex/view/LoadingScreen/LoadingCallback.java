package com.project.nex.view.LoadingScreen;

public interface LoadingCallback {
    void onSuccess();
    void onError(String message);
}
