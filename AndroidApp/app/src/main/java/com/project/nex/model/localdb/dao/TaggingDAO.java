package com.project.nex.model.localdb.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.project.nex.model.localdb.entity.Tagging;

@Dao
public interface TaggingDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertTagging(Tagging tagging);

    @Query("SELECT * FROM tagging WHERE routeplan_id=:id")
    Tagging getTaggingDetails(int id);
}
