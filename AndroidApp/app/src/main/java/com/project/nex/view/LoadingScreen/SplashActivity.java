package com.project.nex.view.LoadingScreen;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import com.project.nex.BuildConfig;
import com.project.nex.R;
import com.project.nex.model.localdb.database.NexDatabase;
import com.project.nex.model.repository.DateTime;
import com.project.nex.util.ConnectionHelper;
import com.project.nex.view.Home.MainActivity;
import com.project.nex.view.Login.LoginActivity;

public class SplashActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        TextView textViewVersion = findViewById(R.id.nexsoft_version);
        textViewVersion.setText("Version " + BuildConfig.VERSION_NAME);
        new Handler().postDelayed(this::check, 1000);
    }

    public void check() {
        DateTime.getTodayDateTime(getApplicationContext());
        ConnectionHelper.checkServerAndDevice(new LoadingCallback() {
            @Override
            public void onSuccess() {
                NexDatabase nexDatabase = Room.databaseBuilder(getApplicationContext(), NexDatabase.class, "dbNex").allowMainThreadQueries().build();
                Intent intent;
                if (nexDatabase.userDAO().getUser() != null) {
                    intent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
                else {
                    intent = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            }

            @Override
            public void onError(String message) {
                if (message.equalsIgnoreCase("Server")) {
                    AlertDialog alertDialog = new AlertDialog.Builder(SplashActivity.this).create();
                    alertDialog.setTitle("Maintenance");
                    alertDialog.setMessage("Server Maintenance");
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", (dialog, which) -> finish());
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.show();
                }
                else {
                    AlertDialog alertDialog = new AlertDialog.Builder(SplashActivity.this).create();
                    alertDialog.setTitle("Error");
                    alertDialog.setMessage("Device not Connected to The Internet");
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", (dialog, which) -> finish());
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.show();
                }
            }
        });
    }
}