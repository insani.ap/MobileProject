package com.project.nex.view.Home.Data.Survey.Details;

import com.project.nex.model.localdb.entity.Survey;

public interface SurveyDetailsView {
    void getFreeText(Survey survey);
    void getRadioButton(Survey survey);
    void getCheckBox(Survey survey);
    void getError();
}
