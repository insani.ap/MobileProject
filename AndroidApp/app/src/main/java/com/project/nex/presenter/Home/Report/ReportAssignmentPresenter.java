package com.project.nex.presenter.Home.Report;

import android.content.Context;
import android.text.InputType;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.room.Room;

import com.project.nex.R;
import com.project.nex.model.localdb.database.NexDatabase;
import com.project.nex.model.localdb.pojo.SurveyReport;
import com.project.nex.model.repository.Tagging.TaggingRepository;
import com.project.nex.view.Home.Report.ReportDetails.ReportAssignmentView;

import java.util.List;

public class ReportAssignmentPresenter {
    private final ReportAssignmentView reportAssignmentView;
    private final Context context;
    public final static int ID_FREE_TEXT = 100;
    public final static int ID_RADIO_BUTTON = 300;
    public final static int ID_CHECK_BOX = 500;
    private LinearLayout.LayoutParams checkBoxConfig;
    private LinearLayout.LayoutParams buttonConfig;

    public ReportAssignmentPresenter(ReportAssignmentView reportAssignmentView, Context context) {
        this.reportAssignmentView = reportAssignmentView;
        this.context = context;
    }

    public void getDetails(String id) {
        NexDatabase nexDatabase = Room.databaseBuilder(context, NexDatabase.class, "dbNex").allowMainThreadQueries().build();
        if (nexDatabase.userDAO().findUserRole().equalsIgnoreCase("Survey")) {
            try {
                config();
                reportAssignmentView.getDataSuccesSurvey(processLayout(Integer.parseInt(id)));
            } catch (Exception e) {
                reportAssignmentView.getDataError();
            }
        }
        else {
            try {
                TaggingRepository taggingRepository = new TaggingRepository(context);
                reportAssignmentView.getDataSuccessTagging(taggingRepository.getTagging(Integer.parseInt(id)));
            } catch (Exception e) {
                reportAssignmentView.getDataError();
            }
        }
    }

    public LinearLayout processLayout(int id) {
        int question = 1;
        LinearLayout linearLayoutMaster = new LinearLayout(context);
        linearLayoutMaster.setOrientation(LinearLayout.VERTICAL);
        NexDatabase nexDatabase = Room.databaseBuilder(context, NexDatabase.class, "dbNex").allowMainThreadQueries().build();
        List<SurveyReport> surveyReports = nexDatabase.surveyReportDAO().getReportByRoutePlan(id);
        for (SurveyReport surveyReport : surveyReports) {
            if (surveyReport.getType().equalsIgnoreCase("FreeText")) {
                linearLayoutMaster.addView(createFreeTextLayout(surveyReport, question));
                question++;
            }
            else if (surveyReport.getType().equalsIgnoreCase("RadioButton")) {
                linearLayoutMaster.addView(createRadioButtonLayout(surveyReport, question));
                question++;
            }
            else if (surveyReport.getType().equalsIgnoreCase("CheckBox")) {
                linearLayoutMaster.addView(createCheckBoxLayout(surveyReport, question));
                question++;
            }
        }
        return linearLayoutMaster;
    }

    public LinearLayout createFreeTextLayout(SurveyReport survey, int question) {
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(LinearLayout.VERTICAL);

        TextView textViewQuestion = new TextView(context);
        EditText editTextAnswer = new EditText(context);

        textViewQuestion.setText("\n"+question+". "+survey.getSurvey_question());
        textViewQuestion.setPadding(12,0,0,0);
        textViewQuestion.setTextSize(18);
        textViewQuestion.setTextColor(context.getResources().getColor(R.color.black));

        editTextAnswer.setText(survey.getAnswer());
        editTextAnswer.setInputType(InputType.TYPE_NULL);

        linearLayout.addView(textViewQuestion);
        linearLayout.addView(editTextAnswer);

        return linearLayout;
    }

    public LinearLayout createRadioButtonLayout(SurveyReport survey, int question) {
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(LinearLayout.VERTICAL);

        TextView textViewQuestion = new TextView(context);

        textViewQuestion.setText("\n"+question+". "+survey.getSurvey_question());
        textViewQuestion.setPadding(12,0,0,0);
        textViewQuestion.setTextSize(18);
        textViewQuestion.setTextColor(context.getResources().getColor(R.color.black));

        linearLayout.addView(textViewQuestion);

        RadioGroup radioGroup = new RadioGroup(context);
        radioGroup.setOrientation(RadioGroup.VERTICAL);

        String[] option = survey.getSurvey_option().split(";");
        String answer = survey.getAnswer();
        RadioButton[] radioButton = new RadioButton[option.length];

        for (int i = 0; i < option.length ; i++) {
            radioButton[i] = new RadioButton(context);
            radioButton[i].setText(option[i]);
            radioButton[i].setTextColor(context.getResources().getColor(R.color.black));
            radioButton[i].setAllCaps(true);
            if (answer.equalsIgnoreCase(option[i])) {
                radioButton[i].setChecked(true);
            }
            radioButton[i].setClickable(false);
            radioGroup.addView(radioButton[i]);
        }

        linearLayout.addView(radioGroup);
        return linearLayout;
    }

    public LinearLayout createCheckBoxLayout(SurveyReport survey, int question) {
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(LinearLayout.VERTICAL);

        TextView textViewQuestion = new TextView(context);

        textViewQuestion.setText("\n"+question+". "+survey.getSurvey_question());
        textViewQuestion.setPadding(12,0,0,0);
        textViewQuestion.setTextSize(18);
        textViewQuestion.setTextColor(context.getResources().getColor(R.color.black));

        linearLayout.addView(textViewQuestion);

        String[] option = survey.getSurvey_option().split(";");
        String[] answer = survey.getAnswer().split(";");
        CheckBox[] checkBoxes = new CheckBox[option.length];

        for (int i = 0; i < option.length ; i++) {
            checkBoxes[i] = new CheckBox(context);
            checkBoxes[i].setText(option[i]);
            checkBoxes[i].setTextColor(context.getResources().getColor(R.color.black));
            checkBoxes[i].setLayoutParams(checkBoxConfig);
            for (String s : answer) {
                if (option[i].equalsIgnoreCase(s)) {
                    checkBoxes[i].setChecked(true);
                }
            }
            checkBoxes[i].setClickable(false);
            linearLayout.addView(checkBoxes[i]);
        }
        return linearLayout;
    }

    public void config() {
        checkBoxConfig = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        buttonConfig = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        buttonConfig.setMargins(0,0,0,64);
    }
}
