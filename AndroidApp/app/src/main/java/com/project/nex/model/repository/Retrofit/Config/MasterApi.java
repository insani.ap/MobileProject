package com.project.nex.model.repository.Retrofit.Config;

import com.project.nex.model.localdb.entity.Customer;
import com.project.nex.model.localdb.entity.Survey;
import com.project.nex.model.localdb.entity.User;
import com.project.nex.model.repository.Retrofit.pojo.RoutePlanRetrofit;
import com.project.nex.model.repository.Retrofit.pojo.SurveyAnswerRetrofit;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface MasterApi {
    //General
    @GET("/datenow")
    Call<String> getDate();

    @GET("/timenow")
    Call<Long> getTime();

    //User
    @POST("/user/auth/sales")
    Call<User> getUser(@Body User user);

    //Customer
    @GET("/customer")
    Call<List<Customer>> getAllCustomer();

    //Survey
    @POST("/survey/user")
    Call<List<Survey>> getALlSurvey(@Body User user);

    //Route Plan
    @POST("/routeplan/getByUser/now")
    Call<List<RoutePlanRetrofit>> getTodayRoutePlan(@Body User user);

    @GET("/routeplan/update/status?")
    Call<String> updateRoutePlanStatus(@Query("id") int id);

    //Survey & Tagging
    @POST("/surveyans/save")
    Call<String> saveRoutePlanSurvey(@Body List<SurveyAnswerRetrofit> surveyAnswerRetrofits);

    @Multipart
    @POST("/tagging/save")
    Call<String> saveRoutePlanTagging(@Part("tagging") RequestBody taggingRetrofit, @Part List<MultipartBody.Part> image);
}