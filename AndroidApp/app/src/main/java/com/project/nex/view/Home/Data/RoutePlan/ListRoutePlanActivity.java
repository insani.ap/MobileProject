package com.project.nex.view.Home.Data.RoutePlan;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.project.nex.R;
import com.project.nex.model.localdb.pojo.RoutePlanNameAddress;
import com.project.nex.presenter.Home.Data.RoutePlan.RoutePlanListPresenter;
import com.project.nex.view.Home.Data.RoutePlan.Adapter.CardRoutePlanAdapter;

import java.util.List;

public class ListRoutePlanActivity extends AppCompatActivity implements RoutePlanView {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_route_plan);
        initRoutePlan();
    }

    public void initRoutePlan() {
        RoutePlanListPresenter routePlanListPresenter = new RoutePlanListPresenter(this, this);
        routePlanListPresenter.getRoutePlanData();
    }

    @Override
    public void getDataSuccess(List<RoutePlanNameAddress> routePlanNameAddresses) {
        RecyclerView recyclerView = findViewById(R.id.list_route_plan_card);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new CardRoutePlanAdapter(routePlanNameAddresses));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
    }

    @Override
    public void getDataError() {
        TextView textView = findViewById(R.id.list_route_plan_no_data);
        textView.setText("NO DATA FOUND!!!");
    }
}
