package com.project.nex.view.Login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.safetynet.SafetyNet;
import com.project.nex.R;
import com.project.nex.presenter.Login.LoginPresenter;
import com.project.nex.view.Home.MainActivity;

public class LoginActivity extends AppCompatActivity implements LoginView {
    private LoginPresenter loginPresenter;
    private CheckBox checkBoxCapthca;
    private EditText username;
    private EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    public void captchaCheck(View view) {
        checkBoxCapthca = (CheckBox) findViewById(R.id.checkbox_captcha);
        SafetyNet.getClient(this).verifyWithRecaptcha("6LeXQWMbAAAAACeztwyRpQMaWc2wIV47zUTzk7dN")
        .addOnSuccessListener(this, response -> checkBoxCapthca.setChecked(true))
        .addOnFailureListener(this, e -> checkBoxCapthca.setChecked(false));
    }

    public void loginCheck(View view) {
        loginPresenter = new LoginPresenter(this);
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        checkBoxCapthca = (CheckBox) findViewById(R.id.checkbox_captcha);
        if (checkBoxCapthca.isChecked()) {
            loginPresenter.login(username.getText().toString(),password.getText().toString(), getApplicationContext());
        }
        else {
            Toast.makeText(this, "Please Agree With Our Terms and Conditions", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void loginSuccess() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra("status", "FirstTime");
        startActivity(intent);
    }

    @Override
    public void loginError(String code) {
        if (code.equalsIgnoreCase("Server Error")) {
            Toast.makeText(this, "Server Error!", Toast.LENGTH_SHORT).show();
        }
        else if (code.equalsIgnoreCase("Not Match") ) {
            Toast.makeText(this, "Username or Password is Wrong!", Toast.LENGTH_SHORT).show();
        }
        else if (code.equalsIgnoreCase("Empty") ) {
            Toast.makeText(this, "Username or Password Should not Empty", Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(this, "Something Error!", Toast.LENGTH_SHORT).show();
        }
    }
}
