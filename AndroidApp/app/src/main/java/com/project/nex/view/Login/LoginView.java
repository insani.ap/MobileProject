package com.project.nex.view.Login;

public interface LoginView {
    void loginSuccess();
    void loginError(String code);
}
