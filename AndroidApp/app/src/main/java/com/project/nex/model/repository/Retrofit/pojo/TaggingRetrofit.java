package com.project.nex.model.repository.Retrofit.pojo;

import com.google.gson.annotations.SerializedName;
import com.project.nex.model.localdb.entity.Customer;
import com.project.nex.model.localdb.entity.User;

public class TaggingRetrofit {
    @SerializedName("id") int id;
    @SerializedName("serialNumber") String serialNumber;
    @SerializedName("urlPhoto") String urlPhoto;
    @SerializedName("nomerKontrak") int nomerKontrak;
    @SerializedName("tglNonaktifAsset") String tglNonaktifAsset;
    @SerializedName("taggingDate") String taggingDate;
    @SerializedName("deleted") boolean deleted = false;
    @SerializedName("customer") Customer customer;
    @SerializedName("user") User user;

    public TaggingRetrofit() {}

    public TaggingRetrofit(int id, String serialNumber, String urlPhoto, int nomerKontrak, String tglNonaktifAsset, String taggingDate, boolean deleted, Customer customer, User user) {
        this.id = id;
        this.serialNumber = serialNumber;
        this.urlPhoto = urlPhoto;
        this.nomerKontrak = nomerKontrak;
        this.tglNonaktifAsset = tglNonaktifAsset;
        this.taggingDate = taggingDate;
        this.deleted = deleted;
        this.customer = customer;
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getUrlPhoto() {
        return urlPhoto;
    }

    public void setUrlPhoto(String urlPhoto) {
        this.urlPhoto = urlPhoto;
    }

    public int getNomerKontrak() {
        return nomerKontrak;
    }

    public void setNomerKontrak(int nomerKontrak) {
        this.nomerKontrak = nomerKontrak;
    }

    public String getTglNonaktifAsset() {
        return tglNonaktifAsset;
    }

    public void setTglNonaktifAsset(String tglNonaktifAsset) {
        this.tglNonaktifAsset = tglNonaktifAsset;
    }

    public String getTaggingDate() {
        return taggingDate;
    }

    public void setTaggingDate(String taggingDate) {
        this.taggingDate = taggingDate;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "TaggingRetrofit{" +
                "id=" + id +
                ", serialNumber='" + serialNumber + '\'' +
                ", urlPhoto='" + urlPhoto + '\'' +
                ", nomerKontrak=" + nomerKontrak +
                ", tglNonaktifAsset='" + tglNonaktifAsset + '\'' +
                ", taggingDate='" + taggingDate + '\'' +
                ", deleted=" + deleted +
                ", customer=" + customer +
                ", user=" + user +
                '}';
    }
}