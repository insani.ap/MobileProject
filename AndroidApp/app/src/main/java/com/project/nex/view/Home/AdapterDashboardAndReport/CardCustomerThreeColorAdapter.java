package com.project.nex.view.Home.AdapterDashboardAndReport;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.project.nex.R;
import com.project.nex.model.localdb.pojo.RoutePlanCustomer;
import com.project.nex.view.Home.Assignment.Survey.SurveyActivity;
import com.project.nex.view.Home.Assignment.Tagging.TaggingActivity;
import com.project.nex.view.Home.Report.ReportDetails.ReportActivity;

import java.util.List;

public class CardCustomerThreeColorAdapter extends RecyclerView.Adapter<CardCustomerThreeColorAdapter.ViewHolder> {
    //View
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView textViewName;
        private TextView textViewId;
        private TextView textViewStatus;
        private ImageView imageViewSatatus;
        private CardView cardView;
        private String header;

        public ViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            this.textViewName = view.findViewById(R.id.card_lc_three_color_customer_name);
            this.textViewId = view.findViewById(R.id.card_lc_three_color_customer_id);
            this.textViewStatus = view.findViewById(R.id.card_lc_three_color_customer_status_hidden);
            this.imageViewSatatus = view.findViewById(R.id.card_lc_three_color_customer_status);
            this.cardView = view.findViewById(R.id.assignment_card_customer);
        }

        @Override
        public void onClick(View view) {
            Intent intent;
            if (this.header.equalsIgnoreCase("SURVEY MENU")) {
                intent = new Intent(view.getContext(), SurveyActivity.class);
            }
            else if (this.header.equalsIgnoreCase("TAGGING MENU")){
                intent = new Intent(view.getContext(), TaggingActivity.class);
            }
            else {
                intent = new Intent(view.getContext(), ReportActivity.class);
            }
            intent.putExtra("name", this.textViewName.getText().toString());
            intent.putExtra("id", this.textViewId.getText().toString());
            intent.putExtra("status", this.header);
            view.getContext().startActivity(intent);
        }
    }

    private Context context;
    private String from;
    private List<RoutePlanCustomer> routePlanCustomers;
    private String header;

    //Constructor
    public CardCustomerThreeColorAdapter(Context context, List<RoutePlanCustomer> routePlanCustomers, String from, String header){
        this.routePlanCustomers = routePlanCustomers;
        this.context = context;
        this.from = from;
        this.header = header;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public CardCustomerThreeColorAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rowItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_lc_three_colors, parent, false);
        return new ViewHolder(rowItem);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(CardCustomerThreeColorAdapter.ViewHolder holder, int position) {
        holder.header = this.header;
        holder.textViewName.setText(this.routePlanCustomers.get(position).getName());
        holder.textViewId.setText(String.valueOf(this.routePlanCustomers.get(position).getId()));
        holder.textViewStatus.setText(this.routePlanCustomers.get(position).getStatus());

        if (this.from.equalsIgnoreCase("From Assignment")) {
            if (this.routePlanCustomers.get(position).getStatus().equalsIgnoreCase("Not Finished")) {
                holder.cardView.setCardBackgroundColor(ContextCompat.getColor(this.context, R.color.red));
                holder.imageViewSatatus.setImageResource(R.drawable.ic_not);
            }
            else {
                holder.cardView.setCardBackgroundColor(ContextCompat.getColor(this.context, R.color.green));
                holder.cardView.setClickable(false);
                holder.imageViewSatatus.setImageResource(R.drawable.ic_report_after);
            }
        }
        else if (this.from.equalsIgnoreCase("From Report")) {
            if (this.routePlanCustomers.get(position).getStatus().equalsIgnoreCase("Not Finished")) {
                holder.cardView.setVisibility(View.GONE);
            }
            else {
                if (this.routePlanCustomers.get(position).getStatus().equalsIgnoreCase("Finished")) {
                    holder.cardView.setCardBackgroundColor(ContextCompat.getColor(this.context, R.color.yellow));
                    holder.imageViewSatatus.setImageResource(R.drawable.ic_report_before);
                }
                else {
                    holder.cardView.setCardBackgroundColor(ContextCompat.getColor(this.context, R.color.green));
                    holder.imageViewSatatus.setImageResource(R.drawable.ic_report_after);
                }
            }
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return this.routePlanCustomers.size();
    }
}