package com.project.nex.presenter.Home.Assignment.Tagging;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;

import com.project.nex.model.repository.Tagging.TaggingRepository;
import com.project.nex.view.Home.Assignment.Tagging.TaggingView;

public class TaggingPresenter {
    private final TaggingView taggingView;
    private final Context context;
    private TaggingRepository taggingRepository;

    public TaggingPresenter (TaggingView taggingView, Context context) {
        this.taggingView = taggingView;
        this.context = context;
        this.taggingRepository = new TaggingRepository(context);
    }

    public void saveTagging(Bitmap photo, String photoName, String serialNumber, String contractNumber, String date, String idRoutePlan) {
        if(photo == null || TextUtils.isEmpty(photoName) || TextUtils.isEmpty(serialNumber) || TextUtils.isEmpty(contractNumber) || TextUtils.isEmpty(date)) {
            taggingView.saveError("Empty");
        }
        else {
            if(taggingRepository.saveTagging(photo, photoName, serialNumber, Integer.parseInt(contractNumber), date, idRoutePlan).equalsIgnoreCase("OK")) {
                taggingView.saveSuccess();
            }
            else {
                taggingView.saveError("Error");
            }
        }
    }
}
