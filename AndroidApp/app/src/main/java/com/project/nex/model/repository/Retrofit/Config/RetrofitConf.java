package com.project.nex.model.repository.Retrofit.Config;

import com.project.nex.util.ConnectionHelper;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitConf {
    private static Retrofit retrofit;

    public static Retrofit init() {
        retrofit = new Retrofit
                .Builder()
                .baseUrl(ConnectionHelper.URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit;
    }
}
