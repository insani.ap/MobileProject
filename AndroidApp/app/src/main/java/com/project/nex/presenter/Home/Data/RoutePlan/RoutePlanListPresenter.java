package com.project.nex.presenter.Home.Data.RoutePlan;

import android.content.Context;

import com.project.nex.model.localdb.pojo.RoutePlanNameAddress;
import com.project.nex.model.repository.RoutePlan.RoutePlanRepository;
import com.project.nex.view.Home.Data.RoutePlan.RoutePlanView;

import java.util.List;

public class RoutePlanListPresenter {
    private final RoutePlanView routePlanView;
    private final Context context;

    public RoutePlanListPresenter(RoutePlanView routePlanView, Context context) {
        this.routePlanView = routePlanView;
        this.context = context;
    }

    public void getRoutePlanData() {
        try {
            RoutePlanRepository routePlanRepository = new RoutePlanRepository(context);
            List<RoutePlanNameAddress> routePlanNameAddresses = routePlanRepository.getAllRoutePlanNameAndAddress();
            routePlanView.getDataSuccess(routePlanNameAddresses);
        } catch (Exception e) {
            routePlanView.getDataError();
        }
    }
}
