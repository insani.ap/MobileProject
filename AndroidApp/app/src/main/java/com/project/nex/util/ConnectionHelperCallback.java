package com.project.nex.util;

public interface ConnectionHelperCallback {
    void onSuccess(String date, Long timeInMilis);
    void onError();
}
