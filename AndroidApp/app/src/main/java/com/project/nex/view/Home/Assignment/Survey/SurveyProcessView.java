package com.project.nex.view.Home.Assignment.Survey;

public interface SurveyProcessView {
    void saveOK();
    void saveFailed(String message);
}
