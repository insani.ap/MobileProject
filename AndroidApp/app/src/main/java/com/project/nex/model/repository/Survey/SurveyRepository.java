package com.project.nex.model.repository.Survey;

import android.content.Context;

import androidx.room.Room;

import com.project.nex.model.localdb.database.NexDatabase;
import com.project.nex.model.localdb.entity.Survey;
import com.project.nex.model.repository.Retrofit.Config.MasterApi;
import com.project.nex.model.repository.Retrofit.Config.RetrofitConf;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SurveyRepository {
    private final NexDatabase nexDatabase;
    private final MasterApi masterApi;

    public SurveyRepository(Context context) {
        nexDatabase = Room.databaseBuilder(context, NexDatabase.class, "dbNex").allowMainThreadQueries().build();
        masterApi = RetrofitConf.init().create(MasterApi.class);
    }

    public void getSurvey(SurveyRepositoryCallback surveyRepositoryCallback) {
        List<Survey> surveys = new ArrayList<>();
        Call<List<Survey>> call = masterApi.getALlSurvey(nexDatabase.userDAO().getUser());
        call.enqueue(new Callback<List<Survey>>() {
            @Override
            public void onResponse(Call<List<Survey>> call, Response<List<Survey>> response) {
                if (response.body() != null) {
                    for (Survey survey: response.body()) {
                        surveys.add(new Survey(survey.getId(), survey.getSurveyQuestion(), survey.getType(), survey.getSurveyOption()));
                    }
                    surveyRepositoryCallback.onSuccess(surveys);
                }
                else {
                    surveyRepositoryCallback.onError();
                }
            }

            @Override
            public void onFailure(Call<List<Survey>> call, Throwable t) {
                surveyRepositoryCallback.onError();
            }
        });
    }

    public String saveSurvey(List<Survey> surveys) {
        try {
            List<Survey> surveyBeforeUpdate = nexDatabase.surveyDAO().getAllSurvey();
            if(compareDataSurvey(surveyBeforeUpdate, surveys)) {
                return "Already Up to Date";
            }
            else {
                if (surveyBeforeUpdate.size() == 0) {
                    nexDatabase.surveyDAO().insertAllSurvey(surveys);
                }
                else {
                    if (surveys.size() >= surveyBeforeUpdate.size()) {
                        for (Survey survey : surveys) {
                            if (nexDatabase.surveyDAO().getDetailsSurvey(survey.getId()) == null) {
                                nexDatabase.surveyDAO().insertSurvey(survey);
                            }
                            else {
                                if (!survey.toString().equals(nexDatabase.surveyDAO().getDetailsSurvey(survey.getId()).toString())) {
                                    nexDatabase.surveyDAO().updateSurvey(survey);
                                }
                            }
                        }
                    }
                    else {
                        nexDatabase.surveyDAO().deleteMissMatch();
                        nexDatabase.surveyDAO().insertAllSurvey(surveys);
                    }
                }
                return "Sync Success";
            }
        } catch (Exception e) {
            return "Something Error";
        }
    }

    public boolean compareDataSurvey(List<Survey> surveysBeforeUpdate, List<Survey> surveysAfterUpdate) {
        StringBuilder stringBuilderBefore = new StringBuilder();
        StringBuilder stringBuilderAfter = new StringBuilder();
        for (Survey sB : surveysBeforeUpdate) {
            stringBuilderBefore.append(sB.toString());
        }
        for (Survey sA : surveysAfterUpdate) {
            stringBuilderAfter.append(sA.toString());
        }
        return stringBuilderBefore.toString().equals(stringBuilderAfter.toString());
    }

    public List<Survey> getAllLocalSurvey() {
        return nexDatabase.surveyDAO().getAllSurvey();
    }

    public Survey getLocalSurveyDetails(int id) {
        return nexDatabase.surveyDAO().getDetailsSurvey(id);
    }

    public int getLocalSurveySize() {
        return nexDatabase.surveyDAO().getAllSurvey().size();
    }
}