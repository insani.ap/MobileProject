package com.project.nex.util;

import android.app.ProgressDialog;
import android.content.Context;

public class ProgressBarHelper {
    public ProgressDialog progressDialog;
    public Context context;

    public ProgressBarHelper(Context context) {
        this.context = context;
        progressDialog = new ProgressDialog(context);
    }

    public void start() {
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage("Loading...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
    }

    public void end() {
        progressDialog.dismiss();
    }
}
