package com.project.nex.model.repository.Customer;

import android.content.Context;

import androidx.room.Room;

import com.project.nex.model.localdb.database.NexDatabase;
import com.project.nex.model.localdb.entity.Customer;
import com.project.nex.model.repository.Retrofit.Config.MasterApi;
import com.project.nex.model.repository.Retrofit.Config.RetrofitConf;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomerRepository {
    private final NexDatabase nexDatabase;
    private final MasterApi masterApi;

    public CustomerRepository (Context context) {
        nexDatabase = Room.databaseBuilder(context, NexDatabase.class, "dbNex").allowMainThreadQueries().build();
        masterApi = RetrofitConf.init().create(MasterApi.class);
    }

    public void getCustomer(CustomerRepositoryCallback customerRepositoryCallback) {
        Call<List<Customer>> call = masterApi.getAllCustomer();
        call.enqueue(new Callback<List<Customer>>() {
            @Override
            public void onResponse(Call<List<Customer>> call, Response<List<Customer>> response) {
                if (response.body() != null) {
                    customerRepositoryCallback.onSuccess(response.body());
                }
                else {
                    customerRepositoryCallback.onError();
                }
            }
            @Override
            public void onFailure(Call<List<Customer>> call, Throwable t) {
                customerRepositoryCallback.onError();
            }
        });
    }

    public String saveCustomer(List<Customer> data) {
        try {
            List<Customer> customersBeforeUpdate = nexDatabase.customerDAO().getAllCustomer();
            if (compareDataCustomer(customersBeforeUpdate, data)) {
                return "Already Up to Date";
            }
            else {
                if (customersBeforeUpdate.size() == 0) {
                    nexDatabase.customerDAO().insertAllCustomer(data);
                }
                else {
                    if (data.size() >= customersBeforeUpdate.size()) {
                        for (Customer customer : data) {
                            if (nexDatabase.customerDAO().getDetailsCustomer(customer.getId()) == null) {
                                nexDatabase.customerDAO().insertCustomer(customer);
                            }
                            else {
                                if (!customer.toString().equals(nexDatabase.customerDAO().getDetailsCustomer(customer.getId()).toString())) {
                                    nexDatabase.customerDAO().updateCustomer(customer);
                                }
                            }
                        }
                    }
                    else {
                        nexDatabase.customerDAO().deleteMissMatch();
                        nexDatabase.customerDAO().insertAllCustomer(data);
                    }
                }
                return "Sync Success";
            }
        } catch (Exception e) {
            return "Something Error";
        }
    }

    public boolean compareDataCustomer(List<Customer> customersBeforeUpdate, List<Customer> customersAfterUpdate) {
        StringBuilder stringBuilderBefore = new StringBuilder();
        StringBuilder stringBuilderAfter = new StringBuilder();
        for (Customer cusB: customersBeforeUpdate) {
            stringBuilderBefore.append(cusB.toString());
        }
        for (Customer cusA: customersAfterUpdate) {
            stringBuilderAfter.append(cusA.toString());
        }
        return stringBuilderBefore.toString().equals(stringBuilderAfter.toString());
    }

    public List<Customer> getAllLocalCustomer() {
        return nexDatabase.customerDAO().getAllCustomer();
    }

    public Customer getDetailsLocalCustomer(String id) {
        return nexDatabase.customerDAO().getDetailsCustomer(Integer.parseInt(id));
    }
}