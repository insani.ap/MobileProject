package com.project.nex.model.repository.RoutePlan;

import android.content.Context;

import androidx.room.Room;

import com.project.nex.model.localdb.database.NexDatabase;
import com.project.nex.model.localdb.entity.RoutePlan;
import com.project.nex.model.localdb.pojo.RoutePlanNameAddress;
import com.project.nex.model.repository.Retrofit.Config.MasterApi;
import com.project.nex.model.repository.Retrofit.Config.RetrofitConf;
import com.project.nex.model.repository.Retrofit.pojo.RoutePlanRetrofit;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RoutePlanRepository {
    private final NexDatabase nexDatabase;
    private final MasterApi masterApi;
    private String date;

    public RoutePlanRepository(Context context) {
        nexDatabase = Room.databaseBuilder(context, NexDatabase.class, "dbNex").allowMainThreadQueries().build();
        masterApi = RetrofitConf.init().create(MasterApi.class);
        date = nexDatabase.todayDateTimeDAO().getDate();
    }

    public void updateRoutePlanStatus(int id) {
        Call<String> call = masterApi.updateRoutePlanStatus(id);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    public void getRoutePlan(RoutePlanRepositoryCallback routePlanRepositoryCallback) {
        List<RoutePlan> routePlans = new ArrayList<>();
        Call<List<RoutePlanRetrofit>> call;
        call = masterApi.getTodayRoutePlan(nexDatabase.userDAO().getUser());
        call.enqueue(new Callback<List<RoutePlanRetrofit>>() {
            @Override
            public void onResponse(Call<List<RoutePlanRetrofit>> call, Response<List<RoutePlanRetrofit>> response) {
                if (response.body() != null) {
                    for (RoutePlanRetrofit routePlanRetrofit : response.body()) {
                        routePlans.add(new RoutePlan(routePlanRetrofit.getId(), routePlanRetrofit.getDate(), routePlanRetrofit.getUser().getId(), routePlanRetrofit.getCustomer().getId()));
                    }
                    routePlanRepositoryCallback.onSuccess(routePlans);
                }
                else {
                    routePlanRepositoryCallback.onError();
                }
            }
            @Override
            public void onFailure(Call<List<RoutePlanRetrofit>> call, Throwable t) {
                routePlanRepositoryCallback.onError();
            }
        });
    }

    public String saveRoutePlan(List<RoutePlan> routePlans) {
        try {
            List<RoutePlan> routePlansBeforeUpdate = nexDatabase.routePlanDAO().getRoutePlan(date);
            if (compareDataRoutePlan(routePlansBeforeUpdate, routePlans)) {
                return "Already Up to Date";
            }
            else {
                if (routePlansBeforeUpdate.size() == 0) {
                    nexDatabase.routePlanDAO().insertAllRoutePlan(routePlans);
                }
                else if (routePlans.size() == routePlansBeforeUpdate.size() || routePlans.size() > routePlansBeforeUpdate.size()) {
                    for (RoutePlan r : routePlans) {
                        if (nexDatabase.routePlanDAO().getOneRoutePlan(String.valueOf(r.getId())) == null) {
                            nexDatabase.routePlanDAO().insertRoutePlan(r);
                        }
                        else {
                            if (!r.toString().equals(nexDatabase.routePlanDAO().getOneRoutePlan(String.valueOf(r.getId())).toString())) {
                                if (nexDatabase.routePlanDAO().getStatus(String.valueOf(r.getId())).equals("Finished")) {
                                    r.setStatus("Finished");
                                }
                                else if (nexDatabase.routePlanDAO().getStatus(String.valueOf(r.getId())).equals("Synced")) {
                                    r.setStatus("Synced");
                                }
                                nexDatabase.routePlanDAO().updateRoutePlan(r);
                            }
                        }
                    }
                }
                else {
                    List<Integer> idAfter = new ArrayList<>();
                    List<Integer> idBefore = nexDatabase.routePlanDAO().getAllIdByDate(date);
                    List<Integer> idDeleted = new ArrayList<>();

                    for (RoutePlan r : routePlans) {
                        idAfter.add(r.getId());
                    }

                    for (Integer i : idBefore) {
                        if (!idAfter.contains(i)) {
                            idDeleted.add(i);
                        }
                    }
                    for (int i = 0; i < idDeleted.size(); i++) {
                        nexDatabase.routePlanDAO().deleteMissMatch(String.valueOf(idDeleted.get(i)));
                    }

                    for (RoutePlan r : routePlans) {
                        if (nexDatabase.routePlanDAO().getOneRoutePlan(String.valueOf(r.getId())) == null) {
                            nexDatabase.routePlanDAO().insertRoutePlan(r);
                        }
                        else {
                            if (!r.toString().equals(nexDatabase.routePlanDAO().getOneRoutePlan(String.valueOf(r.getId())).toString())) {
                                if (nexDatabase.routePlanDAO().getStatus(String.valueOf(r.getId())).equals("Finished")) {
                                    r.setStatus("Finished");
                                }
                                else if (nexDatabase.routePlanDAO().getStatus(String.valueOf(r.getId())).equals("Synced")) {
                                    r.setStatus("Synced");
                                }
                                nexDatabase.routePlanDAO().updateRoutePlan(r);
                            }
                        }
                    }
                }
            }
            return "Sync Success";
        } catch (Exception e) {
            return "Something Error";
        }
    }

    public boolean compareDataRoutePlan(List<RoutePlan> routePlansBeforeUpdate, List<RoutePlan> routePlansAfterUpdate) {
        StringBuilder stringBuilderBefore = new StringBuilder();
        StringBuilder stringBuilderAfter = new StringBuilder();
        for (RoutePlan routePlanB: routePlansBeforeUpdate) {
            stringBuilderBefore.append(routePlanB.toString());
        }
        for (RoutePlan routePlanA: routePlansAfterUpdate) {
            stringBuilderAfter.append(routePlanA.toString());
        }
        return stringBuilderBefore.toString().equals(stringBuilderAfter.toString());
    }

    public String RouteTodayMarked(String status) {
        return String.valueOf(nexDatabase.routePlanDAO().getMarked(date, status));
    }

    public String RouteToday() {
        return String.valueOf(nexDatabase.routePlanDAO().getMark(date));
    }

    public List<RoutePlanNameAddress> getAllRoutePlanNameAndAddress() {
        return nexDatabase.routePlanCustomerDAO().getNameAddressCustomerRoutePlan(date);
    }

    public void updateStatusSynced(int i) {
        nexDatabase.routePlanDAO().updateStatusRoutePlan("Synced", i);
    }
}