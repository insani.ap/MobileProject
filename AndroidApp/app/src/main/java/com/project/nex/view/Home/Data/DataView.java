package com.project.nex.view.Home.Data;

public interface DataView {
    void syncDataSuccess(String message);
    void syncDataError(String message);
}
