package com.project.nex.view.Home.Data.Customer;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.project.nex.R;
import com.project.nex.presenter.Home.Data.Customer.DataListPresenter;
import com.project.nex.view.Home.Data.Customer.Adapter.CardCustomerAdapter;

import java.util.List;

public class ListCustomerActivity extends AppCompatActivity implements DataListCustomerView {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_customer);
        initCustomer();
    }

    private void initCustomer() {
        DataListPresenter dataListPresenter = new DataListPresenter(this);
        dataListPresenter.syncDataFromLocalDb(this);
    }

    @Override
    public void getDataSuccess(List<String> name, List<Integer> id) {
        RecyclerView recyclerView = findViewById(R.id.list_customer_card);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new CardCustomerAdapter(name, id));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
    }

    @Override
    public void getDataError() {
        TextView textView = findViewById(R.id.list_customer_no_data);
        textView.setText("NO DATA FOUND!!!");
    }
}
