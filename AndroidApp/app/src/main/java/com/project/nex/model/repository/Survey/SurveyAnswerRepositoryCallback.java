package com.project.nex.model.repository.Survey;

public interface SurveyAnswerRepositoryCallback {
    void onSuccess();
    void onError(String message);
}
