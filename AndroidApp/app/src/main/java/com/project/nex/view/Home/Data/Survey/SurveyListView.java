package com.project.nex.view.Home.Data.Survey;

import java.util.List;

public interface SurveyListView {
    void getDataSuccess(List<String> question, List<Integer> id);
    void getDataError();
}
