package com.project.nex.view.Home.Assignment.Tagging;

public interface TaggingView {
    void saveSuccess();
    void saveError(String message);
}
