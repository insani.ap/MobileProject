package com.project.nex.view.Home.Assignment;

import com.project.nex.model.localdb.pojo.RoutePlanCustomer;

import java.util.List;

public interface AssignmentView {
    void getDataSuccess(List<RoutePlanCustomer> routePlanCustomers);
    void getDataError();
}
