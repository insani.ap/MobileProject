package com.project.nex.presenter.Home.Data.Survey.Details;

import android.content.Context;

import com.project.nex.model.localdb.entity.Survey;
import com.project.nex.model.repository.Survey.SurveyRepository;
import com.project.nex.view.Home.Data.Survey.Details.SurveyDetailsView;

public class SurveyDetailsPresenter {
    private SurveyDetailsView surveyDetailsView;
    private Context context;

    public SurveyDetailsPresenter(SurveyDetailsView surveyDetailsView, Context context) {
        this.surveyDetailsView = surveyDetailsView;
        this.context = context;
    }

    public void InitDetailsSurvey(String id) {
        SurveyRepository surveyRepository = new SurveyRepository(context);
        Survey survey = surveyRepository.getLocalSurveyDetails(Integer.parseInt(id));
        try {
            if (survey.getType().equalsIgnoreCase("FreeText")) {
                surveyDetailsView.getFreeText(survey);
            }
            else if (survey.getType().equalsIgnoreCase("RadioButton")) {
                surveyDetailsView.getRadioButton(survey);
            }
            else if (survey.getType().equalsIgnoreCase("CheckBox")) {
                surveyDetailsView.getCheckBox(survey);
            }
        } catch (Exception e) {
            surveyDetailsView.getError();
        }
    }
}