package com.project.nex.model.repository.Retrofit.pojo;

import com.google.gson.annotations.SerializedName;
import com.project.nex.model.localdb.entity.Customer;
import com.project.nex.model.localdb.entity.User;

public class RoutePlanRetrofit {
    @SerializedName("id") int id;
    @SerializedName("date") String date;
    @SerializedName("customer") Customer customer;
    @SerializedName("user") User user;

    public RoutePlanRetrofit(int id, String date, Customer customer, User user) {
        this.id = id;
        this.date = date;
        this.customer = customer;
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
