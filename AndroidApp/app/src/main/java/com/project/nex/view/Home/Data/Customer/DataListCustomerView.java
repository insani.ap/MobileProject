package com.project.nex.view.Home.Data.Customer;

import java.util.List;

public interface DataListCustomerView {
    void getDataSuccess(List<String> name, List<Integer> id);
    void getDataError();
}
