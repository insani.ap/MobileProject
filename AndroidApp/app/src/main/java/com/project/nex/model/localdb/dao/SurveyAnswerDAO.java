package com.project.nex.model.localdb.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.project.nex.model.localdb.entity.SurveyAnswer;

import java.util.List;

@Dao
public interface SurveyAnswerDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertSurveyAnswer(SurveyAnswer surveyAnswer);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllSurveyAnswer(List<SurveyAnswer> surveyAnswers);

    @Query("SELECT * FROM survey_answer WHERE route_plan_id=:routePlanId")
    List<SurveyAnswer> getSurveyAnswer(int routePlanId);
}
