package com.project.nex.model.repository.Retrofit.pojo;

import com.google.gson.annotations.SerializedName;
import com.project.nex.model.localdb.entity.Customer;
import com.project.nex.model.localdb.entity.Survey;

public class SurveyAnswerRetrofit {
    @SerializedName("id") int id;
    @SerializedName("answer") String answer;
    @SerializedName("answerDate") String answerDate;
    @SerializedName("survey") Survey survey;
    @SerializedName("customer") Customer customer;

    public SurveyAnswerRetrofit() {}

    public SurveyAnswerRetrofit(int id, String answer, String answerDate, Survey survey, Customer customer) {
        this.id = id;
        this.answer = answer;
        this.answerDate =answerDate;
        this.survey = survey;
        this.customer = customer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Survey getSurvey() {
        return survey;
    }

    public void setSurvey(Survey survey) {
        this.survey = survey;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public String toString() {
        return "SurveyAnswerRetrofit{" +
                "id=" + id +
                ", answer='" + answer + '\'' +
                ", survey=" + survey +
                ", answerDate='" + answerDate + '\'' +
                ", customer=" + customer +
                '}';
    }
}
