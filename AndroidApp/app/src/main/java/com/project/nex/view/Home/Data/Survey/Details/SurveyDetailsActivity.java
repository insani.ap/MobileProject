package com.project.nex.view.Home.Data.Survey.Details;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.project.nex.R;
import com.project.nex.model.localdb.entity.Survey;
import com.project.nex.presenter.Home.Data.Survey.Details.SurveyDetailsPresenter;


public class SurveyDetailsActivity extends AppCompatActivity implements SurveyDetailsView {
    private TextView textViewQuestion;
    private TextView textViewQuestionHeader;
    private TextView textViewType;
    private TextView textViewTypeHeader;
    private TextView textViewOption;
    private TextView textViewOptionHeader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey_details);
        textViewQuestion = findViewById(R.id.question);
        textViewQuestionHeader = findViewById(R.id.question_header);
        textViewType = findViewById(R.id.type);
        textViewTypeHeader = findViewById(R.id.type_header);
        textViewOption = findViewById(R.id.option);
        textViewOptionHeader = findViewById(R.id.option_header);
        initData(getIntent().getStringExtra("id"));
    }

    public void initData(String id) {
        SurveyDetailsPresenter surveyDetailsPresenter = new SurveyDetailsPresenter(this, this);
        surveyDetailsPresenter.InitDetailsSurvey(id);
    }

    @Override
    public void getFreeText(Survey survey) {
        textViewQuestion.setText(survey.getSurveyQuestion());
        textViewType.setText(survey.getType());
        textViewOptionHeader.setVisibility(View.GONE);
        textViewOption.setVisibility(View.GONE);
    }

    @Override
    public void getRadioButton(Survey survey) {
        textViewQuestion.setText(survey.getSurveyQuestion());
        textViewType.setText(survey.getType());
        textViewOption.setText(survey.getSurveyOption().replaceAll(";", "\n"));
    }

    @Override
    public void getCheckBox(Survey survey) {
        textViewQuestion.setText(survey.getSurveyQuestion());
        textViewType.setText(survey.getType());
        textViewOption.setText(survey.getSurveyOption().replaceAll(";", "\n"));
    }

    @Override
    public void getError() {
        textViewQuestion.setVisibility(View.GONE);
        textViewQuestionHeader.setVisibility(View.GONE);
        textViewType.setVisibility(View.GONE);
        textViewTypeHeader.setVisibility(View.GONE);
        textViewOptionHeader.setVisibility(View.GONE);
        textViewOption.setVisibility(View.GONE);
    }
}
