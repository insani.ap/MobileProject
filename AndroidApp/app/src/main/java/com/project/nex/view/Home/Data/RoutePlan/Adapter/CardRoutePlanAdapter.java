package com.project.nex.view.Home.Data.RoutePlan.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.project.nex.R;
import com.project.nex.model.localdb.pojo.RoutePlanNameAddress;

import java.util.List;

public class CardRoutePlanAdapter extends RecyclerView.Adapter<CardRoutePlanAdapter.ViewHolder> {
    //View
    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView textViewName;
        private TextView textViewAddress;

        public ViewHolder(View view) {
            super(view);
            this.textViewName = view.findViewById(R.id.card_rp_name);
            this.textViewAddress = view.findViewById(R.id.card_rp_address);
        }
    }

    private List<RoutePlanNameAddress> routePlanNameAddresses;

    //Constructor
    public CardRoutePlanAdapter(List<RoutePlanNameAddress> routePlanNameAddresses){
        this.routePlanNameAddresses = routePlanNameAddresses;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public CardRoutePlanAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rowItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_rp, parent, false);
        return new ViewHolder(rowItem);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(CardRoutePlanAdapter.ViewHolder holder, int position) {
        holder.textViewName.setText(this.routePlanNameAddresses.get(position).getName());
        holder.textViewAddress.setText(this.routePlanNameAddresses.get(position).getAddress());
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return this.routePlanNameAddresses.size();
    }
}