package com.project.nex.presenter.Home.Data.Customer.Details;

import android.content.Context;

import com.project.nex.model.localdb.entity.Customer;
import com.project.nex.model.repository.Customer.CustomerRepository;
import com.project.nex.view.Home.Data.Customer.Details.CustomerDetailsView;

public class CustomerDetailsPresenter {
    private final CustomerDetailsView customerDetailsView;

    public CustomerDetailsPresenter(CustomerDetailsView customerDetailsView) {
        this.customerDetailsView = customerDetailsView;
    }

    public void details(String id, String name, Context context) {
        CustomerRepository customerRepository = new CustomerRepository(context);
        Customer customer = customerRepository.getDetailsLocalCustomer(id);
        customerDetailsView.show(customer);
    }
}
