package com.project.nex.model.localdb.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "today_date_time")
public class TodayDateTime {
    @ColumnInfo(name = "id") @PrimaryKey int id;
    @ColumnInfo(name = "date") String date;
    @ColumnInfo(name = "time") Long timeInMilis;

    public TodayDateTime() {}

    public TodayDateTime(int id, String date, Long timeInMilis) {
        this.id = id;
        this.date = date;
        this.timeInMilis = timeInMilis;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Long getTimeInMilis() {
        return timeInMilis;
    }

    public void setTimeInMilis(Long timeInMilis) {
        this.timeInMilis = timeInMilis;
    }
}
