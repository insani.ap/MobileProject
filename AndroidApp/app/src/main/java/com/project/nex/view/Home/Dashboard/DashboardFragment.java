package com.project.nex.view.Home.Dashboard;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.project.nex.R;
import com.project.nex.presenter.Home.Dashboard.DashboardPresenter;
import com.project.nex.presenter.Logout.LogoutPresenter;
import com.project.nex.util.ConnectionHelper;
import com.project.nex.view.Home.Dashboard.ProfilePicture.ImagePreviewUser;
import com.project.nex.view.Login.LoginActivity;
import com.project.nex.view.Logout.LogoutView;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import jp.wasabeef.picasso.transformations.CropCircleTransformation;

import static android.widget.Toast.LENGTH_SHORT;

public class DashboardFragment extends Fragment implements LogoutView, DashboardView {
    private TextView dashboardRouteBefore;
    private TextView dashboardRouteAfter;
    private TextView dashboardSync;
    private TextView dashboardName;
    private TextView dashboardId;
    private TextView dashboardDate;
    private ImageView dashBoardImage;
    private String status;

    public DashboardFragment(String status) {
        this.status = status;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        dashboardSync = view.findViewById(R.id.dashboard_route_sync);
        dashboardRouteBefore = view.findViewById(R.id.dashboard_route_before);
        dashboardRouteAfter = view.findViewById(R.id.dashboard_route_after);
        dashboardName = view.findViewById(R.id.dashboard_name);
        dashboardId = view.findViewById(R.id.dashboard_id);
        dashboardDate = view.findViewById(R.id.dashboard_date);
        dashBoardImage = view.findViewById(R.id.dashboard_profile);
        syncCheck();
        Button buttonLogOut = view.findViewById(R.id.dashboard_logout);
        ImageButton buttonSync = view.findViewById(R.id.dashboard_sync_button);
        buttonLogOut.setOnClickListener(v -> logoutCheck());
        buttonSync.setOnClickListener(v -> {
            status = "Not Null";
            AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
            alertDialog.setTitle("Warning!");
            alertDialog.setMessage("Your Synced Data Will Be Deleted");
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", (dialog, which) -> syncCheck());
            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", ((dialog, which) -> alertDialog.cancel()));
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.show();
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        syncCheck();
    }

    public void logoutCheck() {
        LogoutPresenter logoutPresenter = new LogoutPresenter(this);
        logoutPresenter.logout(getActivity());
    }

    public void syncCheck() {
        DashboardPresenter dashboardPresenter = new DashboardPresenter(this, getActivity());
        dashboardPresenter.syncCard(status);
    }

    @Override
    public void logoutSuccess() {
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void logoutFailed() {
        Toast.makeText(getActivity(), "Something Error", LENGTH_SHORT).show();
    }

    @Override
    public void normalCard(String routeToday, String markRoute, String synced, String code) {
        if (null != code) {
            if (code.equalsIgnoreCase("Sync Success")) {
                Toast.makeText(getActivity(), "Sync Success", LENGTH_SHORT).show();
            }
            else if (code.equalsIgnoreCase("Already Up to Date")){
                Toast.makeText(getActivity(), "Already Up to Date", LENGTH_SHORT).show();
            }
        }
        dashboardRouteBefore.setText("Route Plan Today            : "+routeToday);
        dashboardRouteAfter.setText("Route Plan Finished        : "+markRoute);
        dashboardSync.setText("Route Plan Synced          : "+synced);
    }

    @Override
    public void errorSync(String code) {
        if (code.equalsIgnoreCase("Server Error")) {
            Toast.makeText(getActivity(), "Sync Error", LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(getActivity(), "Something Error", LENGTH_SHORT).show();
        }
    }

    @Override
    public void dashboardAdditionalInfo(String name, int id, String role, String urlPhoto) {
        DateFormat dateFormat = new SimpleDateFormat("EEEE, dd MMMM yyyy");
        Date date = new Date();
        dashboardName.setText("Hi "+name);
        dashboardId.setText("Your ID: "+id+" | "+"Your Role: "+role);
        dashboardDate.setText(dateFormat.format(date).toString());
        dashboardDate.setAllCaps(true);
        if (urlPhoto != null) {
            Picasso.get().load(urlPhoto.replace("http://localhost:8080/",ConnectionHelper.URL)).
                    transform(new CropCircleTransformation()).into(dashBoardImage);
        }
        dashBoardImage.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), ImagePreviewUser.class);
            intent.putExtra("image", urlPhoto);
            startActivity(intent);
        });
    }
}