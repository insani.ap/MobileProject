package com.project.nex.view.Home;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.project.nex.R;
import com.project.nex.view.Home.Assignment.AssignmentFragment;
import com.project.nex.view.Home.Dashboard.DashboardFragment;
import com.project.nex.view.Home.Data.DataFragment;
import com.project.nex.view.Home.Report.ReportFragment;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadFragment(new DashboardFragment(getIntent().getStringExtra("status")));
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_nav);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
    }

   @Override
    protected void onResume() {
        super.onResume();
    }

    private boolean loadFragment(Fragment fragment){
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.main_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Fragment fragment = null;
        switch (menuItem.getItemId()){
            case R.id.dashboard_menu:
                fragment = new DashboardFragment(null);
                break;
            case R.id.download_menu:
                fragment = new DataFragment();
                break;
            case R.id.assigment_menu:
                fragment = new AssignmentFragment();
                break;
            case R.id.report_menu:
                fragment = new ReportFragment();
                break;
        }
        return loadFragment(fragment);
    }
}