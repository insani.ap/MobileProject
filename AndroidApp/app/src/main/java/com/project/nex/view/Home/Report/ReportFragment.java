package com.project.nex.view.Home.Report;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.project.nex.R;
import com.project.nex.model.localdb.pojo.RoutePlanCustomer;
import com.project.nex.presenter.Home.Report.ReportPresenter;
import com.project.nex.presenter.Home.Report.ReportSyncPresenter;
import com.project.nex.view.Home.AdapterDashboardAndReport.CardCustomerThreeColorAdapter;

import java.util.List;

public class ReportFragment extends Fragment implements ReportView, ReportSyncView {
    private TextView textViewNoData;
    private RecyclerView recyclerView;
    private FloatingActionButton floatingActionButtonSync;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_report, container, false);
        textViewNoData = view.findViewById(R.id.report_no_data);
        recyclerView = view.findViewById(R.id.report_card);
        floatingActionButtonSync = view.findViewById(R.id.report_floating_button);
        floatingActionButtonSync.setOnClickListener(v -> syncDataToApi());
        initReport();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        initReport();
    }

    public void syncDataToApi() {
        ReportSyncPresenter reportSyncPresenter = new ReportSyncPresenter(this, getActivity());
        reportSyncPresenter.syncData();
    }

    public void initReport() {
        ReportPresenter reportPresenter = new ReportPresenter(this, getActivity());
        reportPresenter.getReportData();
    }

    @Override
    public void getDataSuccess(List<RoutePlanCustomer> routePlanCustomers) {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(new CardCustomerThreeColorAdapter(getActivity(), routePlanCustomers, "From Report", "REPORT MENU"));
    }

    @Override
    public void getDataError() {
        textViewNoData.setText("NO DATA FOUND!!!");
    }

    @Override
    public void syncSuccess(String message) {
        Toast.makeText(getActivity(), "Sync Success", Toast.LENGTH_SHORT).show();
        initReport();
    }

    @Override
    public void syncFailed(String message) {
        if (message.equalsIgnoreCase("Error")) {
            Toast.makeText(getActivity(), "Sync Error", Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(getActivity(), "No Need For Sync", Toast.LENGTH_SHORT).show();
        }
    }
}
