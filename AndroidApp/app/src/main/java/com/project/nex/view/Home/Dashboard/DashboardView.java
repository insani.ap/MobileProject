package com.project.nex.view.Home.Dashboard;

public interface DashboardView {
    void normalCard(String routeToday, String markRoute, String synced,  String code);
    void errorSync(String code);
    void dashboardAdditionalInfo(String name, int id, String role, String urlPhoto);
}
