package com.project.nex.model.localdb.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.project.nex.model.localdb.entity.Survey;

import java.util.List;

@Dao
public interface SurveyDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertSurvey(Survey survey);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllSurvey(List<Survey> surveys);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateSurvey(Survey... surveys);

    @Query("DELETE FROM survey")
    void deleteMissMatch();

    @Query("SELECT * FROM survey")
    List<Survey> getAllSurvey();

    @Query("SELECT * FROM survey WHERE id=:id")
    Survey getDetailsSurvey(int id);
}
