package com.project.nex.view.Home.Assignment.Tagging;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import com.project.nex.R;
import com.project.nex.model.localdb.database.NexDatabase;
import com.project.nex.presenter.Home.Assignment.Tagging.TaggingPresenter;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class TaggingActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, TaggingView {
    private EditText editTextDate;
    private EditText editTextSerialNumber;
    private EditText editTextContractNumber;
    private TextView textViewImageName;
    private ImageView imageView;
    private Bitmap photo;
    private String dateCon;
    private NexDatabase nexDatabase;
    private final int CAMERA_RESULT = 100;
    private final int CAMERA_PERMISSION = 10;
    private final int EX_PERMISSION = 9;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tagging);
        nexDatabase = Room.databaseBuilder(this, NexDatabase.class, "dbNex").allowMainThreadQueries().build();
        imageView = findViewById(R.id.tagging_image);
        textViewImageName = findViewById(R.id.tagging_photo_name);
        editTextDate = findViewById(R.id.tagging_date);
        editTextSerialNumber = findViewById(R.id.tagging_serial_number);
        editTextContractNumber = findViewById(R.id.tagging_contract_number);
        Button buttonSave = findViewById(R.id.tagging_save);
        Button buttonTakePhotos = findViewById(R.id.tagging_button_take_photo);
        editTextDate.setOnClickListener(v -> showDate());
        buttonSave.setOnClickListener(v -> saveData());
        buttonTakePhotos.setOnClickListener(v -> openPhotos());
    }

    public void openPhotos() {
        if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION);
        }
        else {
            Intent takePicture = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(takePicture,CAMERA_RESULT);
        }
    }

    public void saveData() {
        if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, EX_PERMISSION);
        }
        else {
            TaggingPresenter taggingPresenter = new TaggingPresenter(this, this);
            taggingPresenter.saveTagging(photo, textViewImageName.getText().toString(), editTextSerialNumber.getText().toString(), editTextContractNumber.getText().toString(), dateCon, getIntent().getStringExtra("id"));
        }
    }

    public void showDate() {
        int year = Integer.parseInt(nexDatabase.todayDateTimeDAO().getDate().substring(0,4));
        int month = Integer.parseInt(nexDatabase.todayDateTimeDAO().getDate().substring(5,7));
        int day = Integer.parseInt(nexDatabase.todayDateTimeDAO().getDate().substring(8,10));
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, this, year, month-1, day);
        datePickerDialog.setCanceledOnTouchOutside(false);
        datePickerDialog.getDatePicker().setMinDate(nexDatabase.todayDateTimeDAO().getTime());
        datePickerDialog.show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        dateCon = String.valueOf(year)+"-"+String.valueOf(month+1)+"-"+dayOfMonth;
        if (month < 10) {
            dateCon = String.valueOf(year)+"-0"+String.valueOf(month+1)+"-"+dayOfMonth;
            if (dayOfMonth < 10) {
                dateCon = String.valueOf(year)+"-0"+String.valueOf(month+1)+"-0"+dayOfMonth;
            }
        }
        DateFormat dateFormat = new SimpleDateFormat("EEEE, dd MMMM yyyy");
        Date date = new Date(year-1900, month, dayOfMonth);
        editTextDate.setText(dateFormat.format(date));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CAMERA_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent takePicture = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(takePicture,CAMERA_RESULT);
            }
        }
        else if (requestCode == EX_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                TaggingPresenter taggingPresenter = new TaggingPresenter(this, this);
                taggingPresenter.saveTagging(photo, textViewImageName.getText().toString(), editTextSerialNumber.getText().toString(), editTextContractNumber.getText().toString(), dateCon, getIntent().getStringExtra("id"));
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_RESULT && resultCode == Activity.RESULT_OK) {
            photo = (Bitmap) data.getExtras().get("data");
            String imageName = "IMG_"+nexDatabase.todayDateTimeDAO().getDate()+"-"+(int)(Math.random()*9999)+1;
            textViewImageName.setText(imageName);
            imageView.setImageBitmap(photo);
        }
    }

    @Override
    public void saveSuccess() {
        finish();
    }

    @Override
    public void saveError(String code) {
        if (code.equalsIgnoreCase("Empty") ) {
            Toast.makeText(this, "Some Data is Empty", Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(this, "Something Error!", Toast.LENGTH_SHORT).show();
        }
    }
}
