package com.project.nex.view.Home.Data.Customer.Details;

import com.project.nex.model.localdb.entity.Customer;

public interface CustomerDetailsView {
    void show(Customer customer);
}
