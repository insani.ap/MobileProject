package com.project.nex.model.localdb.dao;

import androidx.room.Dao;
import androidx.room.Query;

import com.project.nex.model.localdb.pojo.SurveyReport;

import java.util.List;
@Dao
public interface SurveyReportDAO {
    @Query("SELECT survey_answer.survey_id, survey_answer.answer, " +
            "survey.survey_question, survey.type, survey.survey_option " +
            "FROM survey INNER JOIN survey_answer " +
            "ON survey.id = survey_answer.survey_id " +
            "WHERE survey_answer.route_plan_id=:id")
    public List<SurveyReport> getReportByRoutePlan(int id);
}
