package com.project.nex.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;

public class ImageHelper {
    public static String saveImage(Bitmap bitmap, String fileName) {
        try {
            File path = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "Nexmile" + File.separator + "Images");
            if (!path.exists()){
                path.mkdirs();
            }

            File outFile = new File(path, fileName);
            FileOutputStream outputStream = new FileOutputStream(outFile);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
            outputStream.close();
            return "OK";
        } catch (Exception e) {
            return "Error";
        }
    }

    public static Bitmap getImage(String fileName) {
        try {
            File image = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "Nexmile" + File.separator + "Images" + File.separator + fileName);
            return BitmapFactory.decodeFile(image.getAbsolutePath());
        }
        catch (Exception e) {
            return null;
        }
    }

    public static File getImageFix(String fileName) {
        try {
            return new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "Nexmile" + File.separator + "Images" + File.separator + fileName);
        }
        catch (Exception e) {
            return null;
        }
    }
}
