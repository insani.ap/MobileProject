package com.project.nex.model.localdb.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.project.nex.model.localdb.entity.Customer;

import java.util.List;

@Dao
public interface CustomerDAO {

    @Query("SELECT * FROM customer WHERE id=:id")
    Customer getDetailsCustomer(int id);

    @Query("SELECT * FROM customer")
    List<Customer> getAllCustomer();

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateCustomer(Customer... customers);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertCustomer(Customer customer);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllCustomer(List<Customer> customers);

    @Query("DELETE FROM customer")
    void deleteMissMatch();
}
